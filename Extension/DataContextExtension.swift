//
//  DataContextExtension.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/18.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation
import AlecrimCoreData

extension DataContext {

    var budget: Table<Budget> {

        AlecrimCoreData.DataContext
        return Table<Budget>(dataContext: self)
    }

    var categoryType: Table<CategoryType> {

        AlecrimCoreData.DataContext
        return Table<CategoryType>(dataContext: self)
    }

    var ledger: Table<Ledger> {
        return Table<Ledger>(dataContext: self)
    }

    var bankAccount: Table<BankAccount> {
        return Table<BankAccount>(dataContext: self)
    }

    var exchangeType: Table<ExchangeType> {
        return Table<ExchangeType>(dataContext: self)
    }

    var einvoice: Table<Einvoice> {
        return Table<Einvoice>(dataContext: self)
    }
}
