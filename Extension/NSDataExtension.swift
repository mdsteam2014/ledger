//
//  NSDataExtension.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/17.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation

extension NSData {

    func toColor() -> UIColor {

        var components = [CGFloat](count: 4, repeatedValue:0)
        let length=sizeofValue(components)

        self.getBytes(&components, length: length * components.count)
        let color = UIColor(red: components[0],
                            green: components[1],
                            blue: components[2],
                            alpha: components[3])

        return color
    }
}