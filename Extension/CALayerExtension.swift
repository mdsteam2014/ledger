//
//  CALayerExtension.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/30.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation

extension CALayer {
    var borderUIColor: UIColor {
        set {
            self.borderColor = newValue.CGColor
        }

        get {
            return UIColor(CGColor: self.borderColor!)
        }
    }
}
