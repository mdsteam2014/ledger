//
//  UIView.swift
//  Ledger
//
//  Created by Alex Chen on 2016/5/27.
//  Copyright © 2016 Alex.Chen. All rights reserved.
//

import Foundation

extension UIView {
    class func loadFromNibNamed(nibNamed: String, bundle : NSBundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiateWithOwner(nil, options: nil)[0] as? UIView
    }
}