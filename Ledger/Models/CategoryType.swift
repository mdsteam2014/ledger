//
//  CategoryType.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/4.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import CoreData

class CategoryType: NSManagedObject {
    @NSManaged var id: String?
    @NSManaged var rootId: String?
    @NSManaged var name: String?
    @NSManaged var ledgerType: NSNumber?
    @NSManaged var isRoot: NSNumber?
    @NSManaged var subCategoryCount: NSNumber?
    @NSManaged var hasSubCategory: NSNumber?
    @NSManaged var index: NSNumber?
    @NSManaged var typeColor: NSData?
}