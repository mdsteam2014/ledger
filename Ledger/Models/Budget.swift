//
//  Budget.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/16.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import CoreData

class Budget: NSManagedObject {
    @NSManaged var categoryId: String?
    @NSManaged var amount: NSNumber?
}