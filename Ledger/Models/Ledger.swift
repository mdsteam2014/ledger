//
//  Ledger.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/3.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation
import CoreData

class Ledger: NSManagedObject {
    @NSManaged var amount: NSNumber?
    @NSManaged var date: NSDate?
    @NSManaged var bankId: String?
    @NSManaged var einvoiceNum: String?
    @NSManaged var ledgerType: NSNumber?
    @NSManaged var categoryTypeId: String?
    @NSManaged var categoryTypeRootId: String?
    @NSManaged var exchangeTypeId: String?
    @NSManaged var note: String?
}