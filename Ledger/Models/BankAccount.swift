//
//  BankAccount.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/13.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation
import CoreData


class BankAccount: NSManagedObject {

    @NSManaged var amount: NSNumber?
    @NSManaged var balanceDate: NSDate?
    @NSManaged var bankName: String?
    @NSManaged var id: String?
    @NSManaged var imageNmae: String?
    @NSManaged var totalAmount: NSNumber?
    @NSManaged var isDefault: NSNumber?
    @NSManaged var index: NSNumber?
    @NSManaged var accountColor: NSData?
}
