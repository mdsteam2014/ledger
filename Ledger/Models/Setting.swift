//
//  Setting.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/12.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation
import CoreData


class Setting: NSManagedObject {


    @NSManaged var currencyType: NSNumber?
    @NSManaged var hasPassword: NSNumber?
    @NSManaged var monthStartDate: NSNumber?
    @NSManaged var weekStartDate: NSNumber?
}
