//
//  ExpenseExchangeType.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/12.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation
import CoreData


class ExchangeType: NSManagedObject {
    @NSManaged var id: String?
    @NSManaged var name: String?
}
