//
//  Einvoice.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/17.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation
import CoreData


class Einvoice: NSManagedObject {

    @NSManaged var amount: NSNumber?
    @NSManaged var cardNo: NSNumber?
    @NSManaged var cardType: String?
    @NSManaged var invDate: NSDate?
    @NSManaged var invDonatable: NSNumber?
    @NSManaged var invNum: String?
    @NSManaged var invPeriod: String?
    @NSManaged var invStatus: NSNumber?
    @NSManaged var sellerName: String?
    @NSManaged var isUpdate: NSNumber?
}
