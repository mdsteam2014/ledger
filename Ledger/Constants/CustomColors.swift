//
//  CustomColors.swift
//  Ledger
//
//  Created by Alex Chen on 2016/5/30.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation


struct CustomColors {
    static let alphaBackgroundColor = UIColor(red: 26/255, green: 34/255, blue: 39/255, alpha: 0.9)
    static let backgroundColor = UIColor(red: 26/255, green: 34/255, blue: 39/255, alpha: 1)
    static let tabBarTintColor = UIColor(red: 70/255, green: 194/255, blue: 242/255, alpha: 1)
    static let menuBackgroundColor = UIColor(red: 251.0/255, green: 46.0/255, blue: 12.0/255, alpha: 1)
    static let homeAddButtonBackgroundColor = UIColor(red: 50/255, green: 110/255, blue: 243/255, alpha: 0.9)
    static let budgetTabelSelectedBackgroundColor = UIColor(red: 251.0/255, green: 46.0/255, blue: 12.0/255, alpha: 0.1)
    static let addItemCellBackgroundColor = UIColor(red: 28.0/255, green: 28.0/255, blue: 29.0/255, alpha: 1)
    static let itemSeparatorBackgroundColor = UIColor(red: 49.0/255, green: 49.0/255, blue: 50.0/255, alpha: 1)
    static let itemSeparatorWithAlphaBackgroundColor = UIColor(red: 49.0/255, green: 49.0/255, blue: 50.0/255, alpha: 0.4)
    static let CalculatorDownColor = UIColor(red: 32/255, green: 147/255, blue: 173/255, alpha: 1)
    static let CalculatorOriginColor = UIColor(red: 22/255, green: 100/255, blue: 118/255, alpha: 1)

    static let SwitchTintColorColor = UIColor(red: 62/255, green: 193/255, blue: 244/255, alpha: 1)
}