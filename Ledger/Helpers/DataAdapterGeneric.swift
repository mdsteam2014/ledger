//
//  DataSet.swift
//  Services
//
//  Created by Alex Chen on 2016/1/15.
//  Copyright © 2016 bsquare. All rights reserved.
//

import Foundation
import CoreData

public class DataAdapterGeneric<T: NSManagedObject> {

    let mOperationQueue = NSOperationQueue()

    var managedObjectContext: NSManagedObjectContext!

    init(content: NSManagedObjectContext) {
        managedObjectContext = content
    }


    // Create a new entity of the default object
    public func createEntity() -> T {
        return NSEntityDescription.insertNewObjectForEntityForName(genericClassName(), inManagedObjectContext: self.managedObjectContext) as! T
    }

    // Saves changes to all entities
    public func saveEntities() {
        // Business rules passed
        if self.managedObjectContext.hasChanges {
            do {
                try self.managedObjectContext.save()
            } catch let error {
                print(error)
            }
        }
    }

    private func fetchEntities(request: NSFetchRequest) -> [T]? {
        do {
            if let fetchResult = try self.managedObjectContext.executeFetchRequest(request) as? [T] {
                return fetchResult
            } else {
                return nil
            }
        } catch {
            print(error)
            return nil
        }
    }

    // Gets all entities of the default type or by query
    public func getAllEntities(aQuery: NSPredicate) -> [T]? {

        let entityName = genericClassName()
        // Create the request object
        let request : NSFetchRequest = NSFetchRequest(entityName: entityName)
        request.predicate = aQuery
        return fetchEntities(request)
    }

    // Gets all entities of the default type
    public func getAllEntities() -> [T]? {
        let entityName = genericClassName()
        // Create the request object
        let request : NSFetchRequest = NSFetchRequest(entityName: entityName)
        return fetchEntities(request)
    }

    // Mark the specified entity for deletion
    public func deleteEntity(entity: NSManagedObject) {
        self.managedObjectContext.deleteObject(entity)
        do {
            try self.managedObjectContext.save()
        } catch {
            print(error)
        }
    }

    // Mark the entity of items for deletion
    public func deleteEntities() {
        let fetchRequest = NSFetchRequest(entityName: self.genericClassName())
        do {
            if let fetchResults = try self.managedObjectContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
                for results in fetchResults {
                    self.managedObjectContext.deleteObject(results)
                    // Save
                    try self.managedObjectContext.save()
                }
            }
        } catch {
            print(error)
        }
    }

    // Get generic class name
    private func genericClassName() -> String {
        let fullName: String = NSStringFromClass(T.self)
        let range = fullName.rangeOfString(".", options: .BackwardsSearch)
        if let range = range {
            return fullName.substringFromIndex(range.endIndex)
        } else {
            return fullName
        }
    }

    // Get generic class name by type
    private func genericClassName(aType: AnyClass?) -> String {

        if let type = aType {
            let fullName: String = NSStringFromClass(type)
            let range = fullName.rangeOfString(".", options: .BackwardsSearch)
            if let range = range {
                return fullName.substringFromIndex(range.endIndex)
            } else {
                return fullName
            }
        } else {
            return ""
        }
    }
}
