//
//  File.swift
//  Services
//
//  Created by Alex Chen on 2016/1/18.
//  Copyright © 2016 bsquare. All rights reserved.
//

import Foundation
import CoreData
import AlecrimCoreData

public class CoreDataHelper {

    public class var sharedInstance: CoreDataHelper {
        struct Singleton {
            static let instance = CoreDataHelper()
        }
        return Singleton.instance
    }

    var dataContext: DataContext!

    init() {
        dataContext = DataContext(dataContextOptions: (UIApplication.sharedApplication().delegate as! AppDelegate).getContextOptions())
    }


    func getLedgerByDateRang(startDate: NSDate, endDate: NSDate, type: Int) -> [Ledger] {

        let items = self.dataContext.ledger.filter({ $0.date!.isLaterOrEqualThanDate(startDate) && $0.date!.isEarlierOrEqualThanDate(endDate) && $0.ledgerType == type})

        return items
    }

    func getBudgetByMonth(date: NSDate) -> [Ledger] {

        return self.dataContext.ledger.filter({ $0.date!.isEqualToDateIgnoringDayAndTime(date) && $0.ledgerType == 0 })
    }

    func getCategoryTypeById(id: String) -> [CategoryType] {
        return self.dataContext.categoryType.filter({ $0.id == id })
    }

    func preloadData() {

        let path = NSBundle.mainBundle().pathForResource("preloadData", ofType: "json")
        let data = NSData(contentsOfFile: path!)
        let items = JSON(data: data!)

        var index = 0
        var subIndex = 0
        for item in items["ExpenseCategoryType"].dictionaryValue {
            let rootType = dataContext.categoryType.createEntity()
            rootType.id = NSUUID().UUIDString
            rootType.name = item.0
            rootType.ledgerType = 0
            rootType.isRoot = true
            rootType.index = index
            rootType.typeColor = UIColor().getRandomColor().toData()
            index += 1
            var count = 0
            subIndex = 0
            for itm in item.1.arrayValue {

                subIndex += 1
                count += 1
                let type = dataContext.categoryType.createEntity()
                type.id = NSUUID().UUIDString
                type.name = itm.stringValue
                type.ledgerType = 0
                type.rootId = rootType.id
                rootType.index = subIndex
            }

            rootType.hasSubCategory = count > 0
            rootType.subCategoryCount = count
        }

        index = 0
        subIndex = 0

        for item in items["IncomeCategoryType"].dictionaryValue {
            let rootType = dataContext.categoryType.createEntity()
            rootType.id = NSUUID().UUIDString
            rootType.name = item.0
            rootType.ledgerType = 1
            rootType.isRoot = true
            rootType.index = index
            rootType.typeColor = UIColor().getRandomColor().toData()
            index += 1
            var count = 0
            subIndex = 0
            for itm in item.1.arrayValue {

                let type = dataContext.categoryType.createEntity()
                type.id = NSUUID().UUIDString
                type.name = itm.stringValue
                type.ledgerType = 1
                type.rootId = rootType.id
                rootType.index = subIndex
                subIndex += 1
                count += 1
            }
            rootType.hasSubCategory = count > 0
            rootType.subCategoryCount = count
        }

        for item in items["ExchangeType"].arrayValue {
            let type = dataContext.exchangeType.createEntity()
            type.id = NSUUID().UUIDString
            type.name = item.stringValue

        }

        let bank = dataContext.bankAccount.createEntity()
        bank.amount = 0
        bank.imageNmae = "bank"
        bank.isDefault = true
        bank.bankName = "預設帳戶"
        bank.id = NSUUID().UUIDString
        bank.index = 0
        bank.accountColor = UIColor().getRandomColor().toData()
        
        do {
            try self.dataContext.save()
        } catch {
            print(error)
        }
    }
}
