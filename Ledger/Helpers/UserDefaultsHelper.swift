//
//  UserDefaultsHelper.swift
//  Ledger
//
//  Created by Alex Chen on 2016/4/8.
//  Copyright © 2016 Alex.Chen. All rights reserved.
//

import Foundation

class UserDefaultsHelper {

    class var sharedInstance: UserDefaultsHelper {
        struct Singleton {
            static let instance = UserDefaultsHelper()
        }
        return Singleton.instance
    }

    let isPreloadedKey = "isPreloaded"
    let analysisSegmentIndexKey = "analysisSegmentIndex"
    let analysisCustomStartDateKey = "analysisCustomStartDate"
    let analysisCustomEndDateKey = "analysisCustomEndDateKey"

    let defaults = NSUserDefaults.standardUserDefaults()

    func isPreloadedCoreData(isPreloaded: Bool) {

        defaults.setBool(isPreloaded, forKey: isPreloadedKey)
    }

    func setAnalysisCustomStartDate(startDate: NSDate) {
        if isKeyAlreadyExist(analysisCustomStartDateKey) {
            defaults.setObject(startDate, forKey: analysisCustomStartDateKey)
        } else {
            defaults.setObject(NSDate(), forKey: analysisCustomStartDateKey)
        }
    }

    func getAnalysisCustomStartDate() -> NSDate {
        if isKeyAlreadyExist(analysisCustomStartDateKey) {
            return defaults.objectForKey(analysisCustomStartDateKey) as! NSDate
        } else {
            defaults.setObject(NSDate(), forKey: analysisCustomStartDateKey)
            return NSDate()
        }
    }

    func setAnalysisCustomEndDate(endDate: NSDate) {
        if isKeyAlreadyExist(analysisCustomEndDateKey) {
            defaults.setObject(endDate, forKey: analysisCustomEndDateKey)
        } else {
            defaults.setObject(NSDate(), forKey: analysisCustomEndDateKey)
        }
    }

    func getAnalysisCustomEndDate() -> NSDate {
        if isKeyAlreadyExist(analysisCustomEndDateKey) {
            return defaults.objectForKey(analysisCustomEndDateKey) as! NSDate
        } else {
            defaults.setObject(NSDate(), forKey: analysisCustomEndDateKey)
            return NSDate()
        }
    }

    func setAnalysisSegmentIndex(index: Int) {
        if isKeyAlreadyExist(analysisSegmentIndexKey) {
            defaults.setInteger(index, forKey: analysisSegmentIndexKey)
        } else {
            defaults.setInteger(2, forKey: analysisSegmentIndexKey)
        }
    }

    func getAnalysisSegmentIndex() -> Int {
        if isKeyAlreadyExist(analysisSegmentIndexKey) {
           return defaults.integerForKey(analysisSegmentIndexKey)
        } else {
            defaults.setInteger(4, forKey: analysisSegmentIndexKey)
            return 4
        }
    }

    func isKeyAlreadyExist(key: String) -> Bool {
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()

        if (userDefaults.objectForKey(key) != nil) {
            return true
        }
        return false
    }
}