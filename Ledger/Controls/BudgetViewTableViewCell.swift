//
//  BudgetViewTableViewCell.swift
//  Ledger
//
//  Created by Alex Chen on 2016/5/31.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

class BudgetViewTableViewCell: UITableViewCell {

    @IBOutlet weak var percentageRing: MKRingProgressView!
    @IBOutlet weak var expenseAmountLabel: UILabel!
    @IBOutlet weak var budgetAmountLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCategoryColor(color: UIColor) {
        self.percentageRing.startColor = color
        self.typeLabel.textColor = color
    }
    
}
