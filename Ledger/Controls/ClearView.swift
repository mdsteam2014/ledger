//
//  ClearView.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/2.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation

class ClearView : UIView {

    var textField : UITextField? {
        didSet {
            self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ClearView.clearButtonPressed(_:))))
        }
    }

    var buttonBackgroundColor = UIColor.grayColor()
    var buttonXColor = CustomColors.backgroundColor
    let X_PADDING_PERCENT : CGFloat = 0.3
    let LINE_WIDTH : CGFloat = 2.0

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.clearColor()
    }

    @objc private func clearButtonPressed(sender: AnyObject) {
        if let tf = self.textField {
            tf.text = ""
        }
    }

    override func drawRect(rect: CGRect) {
        buttonBackgroundColor.set()

        // Background
        UIBezierPath(roundedRect: self.bounds, cornerRadius: self.bounds.size.width / 2.0).fill()

        buttonXColor.set()

        // First slash: \
        let firstSlash = UIBezierPath()
        styleBezierPath(firstSlash)

        // Give the 'X' a bit of padding from the bounds
        let slashPadding = self.bounds.size.width * X_PADDING_PERCENT

        // Start in the upper-left
        var topLeft = CGPointMake(self.bounds.origin.x + slashPadding, self.bounds.origin.y + slashPadding)
        firstSlash.moveToPoint(topLeft)
        // Slide down, and to the right
        topLeft.x = self.bounds.origin.x + self.bounds.size.width - slashPadding
        topLeft.y = self.bounds.origin.y + self.bounds.size.height - slashPadding
        firstSlash.addLineToPoint(topLeft)
        firstSlash.stroke()

        // Create a copy of the first slash: \
        let secondSlash = UIBezierPath(CGPath: firstSlash.CGPath)
        styleBezierPath(secondSlash)
        // Mirror the slash over the Y axis: /
        let mirrorTransform = CGAffineTransformMakeScale(1.0, -1.0);
        secondSlash.applyTransform(mirrorTransform)
        // And translate ("move") the path to intersect the first slash
        let translateOverY = CGAffineTransformMakeTranslation(0.0, self.bounds.size.height)
        secondSlash.applyTransform(translateOverY)
        secondSlash.stroke()
    }

    private func styleBezierPath(path: UIBezierPath) {
        path.lineWidth = LINE_WIDTH
        path.lineCapStyle = CGLineCap.Round
    }
}