//
//  CustomDateView.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/25.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation

class CustomDateView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)

        let label = UILabel()
        label.text = "起始日期"
        self.addSubview(label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}