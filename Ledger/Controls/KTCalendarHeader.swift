//
//  KTCalendarHeader.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/25.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation

protocol KTCalendarHeaderDelegate {
    func headerScrollViewDidEndDecelerating(index: Int)
}

class KTCalendarHeader: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {

    var collectionView: UICollectionView!
    var horizontalLinearFlowLayout: KTHorizontalLinearFlowLayout = KTHorizontalLinearFlowLayout()

    var dateStrings: [String] = [String]()

    var minimumDate: NSDate = NSDate()
    var maximumDate: NSDate = NSDate()

    var delegate: KTCalendarHeaderDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.maximumDate = NSDate().dateWithYear(2099, month: 12, day: 31)
        self.minimumDate = NSDate().dateWithYear(1970, month: 1, day: 1)

        collectionView = UICollectionView(frame: CGRectMake(0, 0, self.frame.width, self.frame.height), collectionViewLayout: horizontalLinearFlowLayout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerNib(UINib(nibName: "CalendarlCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CalendarlCollectionViewCell")
        collectionView.backgroundColor = UIColor.clearColor()
        collectionView.showsHorizontalScrollIndicator = false
        self.horizontalLinearFlowLayout = KTHorizontalLinearFlowLayout.configureLayout(collectionView: collectionView, itemSize: CGSizeMake(150, 40), minimumLineSpacing: 0)

        self.addSubview(collectionView)

        self.collectionView.performBatchUpdates({

            let item = self.collectionView(self.collectionView!, numberOfItemsInSection: 0) - 1
            self.selectedIndex = NSIndexPath(forItem: item, inSection: 0)

            self.collectionView?.scrollToItemAtIndexPath(self.selectedIndex!, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
            }, completion: nil)

    }

    var selectedIndex: NSIndexPath?

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    func resetNewOffset(index: Int) {
        let lastItemIndex = NSIndexPath(forItem: index, inSection: 0)
        self.collectionView?.scrollToItemAtIndexPath(lastItemIndex, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
    }

    // MARK: - Collection Delegate

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let minimumPage = NSDate().beginingOfMonthOfDate(NSDate().dateBySubtractingMonths(24))
        let count = NSDate().monthsFromDate(minimumPage, toDate: NSDate()) + 1
        return count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let date = NSDate().dateByAddingMonths(indexPath.row, toDate: NSDate().dateBySubtractingMonths(24))
        let startDate = date.dateAtTheStartOfMonth().toString(format: DateFormat.Custom("yyyy年MM月份"))
        let collectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("CalendarlCollectionViewCell", forIndexPath: indexPath) as! CalendarlCollectionViewCell
        collectionViewCell.dateLabel.text = "\(startDate)"
        //collectionViewCell.yearLabel.text = date.toString(format: DateFormat.Custom("yyyy"))
        collectionViewCell.header = self
        return collectionViewCell
    }

    //MARK: - UIScrollViewDelegate

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {

        let visibleRect = CGRect(origin:  self.collectionView.contentOffset, size: self.collectionView.bounds.size)
        let visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect))
        if let visibleIndexPath = self.collectionView.indexPathForItemAtPoint(visiblePoint) {
            
            delegate?.headerScrollViewDidEndDecelerating(visibleIndexPath.row)
        }
        
    }
}