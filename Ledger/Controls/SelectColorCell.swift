//
//  SelectColorCell.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/23.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

class SelectColorCell: UITableViewCell {

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    var onColorSelected: (UIColor -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = "選擇顏色"
        self.backgroundColor = CustomColors.addItemCellBackgroundColor
        selectionStyle = .None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    override func select(sender: AnyObject?) {

    }

    func setColor(color: UIColor) {
        colorView.backgroundColor = color
    }
}
