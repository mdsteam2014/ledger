//
//  CalendarlCollectionViewCell.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/20.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

class CalendarlCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var dateLabel: UILabel!

    var header: KTCalendarHeader?

    override func awakeFromNib() {
        super.awakeFromNib()
        dateLabel.textColor = UIColor.whiteColor()
    }
}
