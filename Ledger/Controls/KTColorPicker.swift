//
//  KTColorPicker.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/23.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

internal protocol KTColorPickerDelegate : NSObjectProtocol {
    func KTColorColorPickerTouched(sender:KTColorPicker, color:UIColor, point:CGPoint, state:UIGestureRecognizerState)
}

class KTColorPicker: UIView {

    weak internal var delegate: KTColorPickerDelegate?
    let saturationExponentTop:Float = 2.0
    let saturationExponentBottom:Float = 1.3
    let thumb: UIView = UIView()

    @IBInspectable var elementSize: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }

    private func initialize() {
        self.clipsToBounds = true
        let touchGesture = UILongPressGestureRecognizer(target: self, action: #selector(KTColorPicker.touchedColor(_:)))
        touchGesture.minimumPressDuration = 0
        touchGesture.allowableMovement = CGFloat.max
        self.addGestureRecognizer(touchGesture)

        thumb.frame = CGRectMake(0, 0, 10, 10)
        thumb.layer.borderColor = UIColor.grayColor().CGColor
        thumb.layer.borderWidth = 2
        thumb.layer.cornerRadius = 5
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func setColor(color: UIColor) {

        let point = self.getPointForColor(color)
        thumb.frame = CGRectMake(point.x, point.y, 10, 10)
    }


    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()

        for y in (0 as CGFloat).stride(to: rect.height, by: elementSize) {

            var saturation = y < rect.height / 2.0 ? CGFloat(2 * y) / rect.height : 2.0 * CGFloat(rect.height - y) / rect.height
            saturation = CGFloat(powf(Float(saturation), y < rect.height / 2.0 ? saturationExponentTop : saturationExponentBottom))
            let brightness = y < rect.height / 2.0 ? CGFloat(1.0) : 2.0 * CGFloat(rect.height - y) / rect.height

            for x in (0 as CGFloat).stride(to: rect.width, by: elementSize) {
                let hue = x / rect.width
                if saturation != 0.0 {
                    let color = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
                    CGContextSetFillColorWithColor(context, color.CGColor)
                    CGContextFillRect(context, CGRect(x:x, y:y, width:elementSize,height:elementSize))
                }
            }
        }

        self.addSubview(thumb)
    }

    func getColorAtPoint(point:CGPoint) -> UIColor {
        let roundedPoint = CGPoint(x:elementSize * CGFloat(Int(point.x / elementSize)),
                                   y:elementSize * CGFloat(Int(point.y / elementSize)))
        var saturation = roundedPoint.y < self.bounds.height / 2.0 ? CGFloat(2 * roundedPoint.y) / self.bounds.height
            : 2.0 * CGFloat(self.bounds.height - roundedPoint.y) / self.bounds.height
        saturation = CGFloat(powf(Float(saturation), roundedPoint.y < self.bounds.height / 2.0 ? saturationExponentTop : saturationExponentBottom))
        let brightness = roundedPoint.y < self.bounds.height / 2.0 ? CGFloat(1.0) : 2.0 * CGFloat(self.bounds.height - roundedPoint.y) / self.bounds.height
        let hue = roundedPoint.x / self.bounds.width
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }

    func getPointForColor(color:UIColor) -> CGPoint {
        var hue:CGFloat=0;
        var saturation:CGFloat=0;
        var brightness:CGFloat=0;
        color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: nil);

        var yPos:CGFloat = 0
        let halfHeight = (self.bounds.height / 2)

        if (brightness >= 0.99) {
            let percentageY = powf(Float(saturation), 1.0 / saturationExponentTop)
            yPos = CGFloat(percentageY) * halfHeight
        } else {
            //use brightness to get Y
            yPos = halfHeight + halfHeight * (1.0 - brightness)
        }

        let xPos = hue * self.bounds.width

        return CGPoint(x: xPos, y: yPos)
    }

    func touchedColor(gestureRecognizer: UILongPressGestureRecognizer){
        let point = gestureRecognizer.locationInView(self)
        let color = getColorAtPoint(point)

        if point.y > 0 && point.x > 0 {
            thumb.frame = CGRectMake(point.x, point.y, 10, 10)
        }
        
        self.delegate?.KTColorColorPickerTouched(self, color: color, point: point, state:gestureRecognizer.state)
    }
}
