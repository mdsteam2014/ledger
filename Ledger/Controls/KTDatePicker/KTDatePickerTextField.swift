//
//  KTDatePickerTextField.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/27.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

@IBDesignable
class KTDatePickerTextField: UIView {

    var view: UIView!
    @IBOutlet weak var startTextField: UITextField!
    @IBOutlet weak var endTextField: UITextField!
    override init(frame: CGRect) {
        // 1. setup any properties here

        // 2. call super.init(frame:)
        super.init(frame: frame)

        // 3. Setup view from .xib file
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here

        // 2. call super.init(coder:)
        super.init(coder: aDecoder)

        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)

        self.endTextField.layer.cornerRadius = frame.height / 2
        self.endTextField.layer.borderColor = UIColor.grayColor().CGColor
        self.endTextField.layer.borderWidth = 1
        self.endTextField.backgroundColor = CustomColors.itemSeparatorBackgroundColor

        self.startTextField.layer.cornerRadius = frame.height / 2
        //self.startTextField.layer.borderColor = UIColor.grayColor().CGColor
        self.startTextField.layer.borderWidth = 1
        self.startTextField.backgroundColor = UIColor.redColor()



    }

    func loadViewFromNib() -> UIView {

        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "KTDatePickerTextField", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }

    func datePickerValueChanged(sender:UIDatePicker) {

        let dateFormatter = NSDateFormatter()

        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle

        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle

        startTextField.text = dateFormatter.stringFromDate(sender.date)
        
    }

    @IBAction func editDidBegin(sender: AnyObject) {

        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date

        (sender as! UITextField).inputView = datePickerView

        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor.blackColor()
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(dismissPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)

        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        startTextField.inputAccessoryView = toolBar
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
    }

    func dismissPicker() {
        view.endEditing(true)
    }

}