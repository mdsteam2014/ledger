//
//  BudgetViewCollectionViewCell.swift
//  Ledger
//
//  Created by Alex Chen on 2016/5/31.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

protocol BudgetCollectionDelegate {
    func budgetCollectionSelectedItem(indexPath: NSIndexPath, categoryType: CategoryType)
}

class BudgetViewCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var darkGrayBackgroundView = UIView()

    var items: [Ledger]?

    var typeItems: [CategoryType] = []

    var delgate: BudgetCollectionDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()

        darkGrayBackgroundView.backgroundColor = UIColor.darkGrayColor()
        self.tableView.registerNib(UINib(nibName: "BudgetViewTableViewCell", bundle: nil), forCellReuseIdentifier: "budgetViewTableViewCell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.allowsSelection = true
        typeItems = CoreDataHelper.sharedInstance.dataContext.categoryType.filter({ $0.isRoot == true })
    }

    func reloadTableView() {
        self.tableView.reloadData()
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        delgate?.budgetCollectionSelectedItem(indexPath, categoryType: self.typeItems[indexPath.row])
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return typeItems.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        var total = 0.0
        var expenseTotal = 0.0
        var budgetAmount = 0.0

        let cell = tableView.dequeueReusableCellWithIdentifier("budgetViewTableViewCell", forIndexPath: indexPath) as! BudgetViewTableViewCell

        let typeId = typeItems[indexPath.row].id
        let subTypes = CoreDataHelper.sharedInstance.dataContext.categoryType.filter({ $0.rootId == typeId})
        for type in subTypes {
            if let i = items!.filter({$0.categoryTypeId! == type.id}).first {
                expenseTotal += i.amount!.doubleValue
            }
        }
        if let budget = CoreDataHelper.sharedInstance.dataContext.budget.filter({$0.categoryId == typeId}).first {
            budgetAmount = budget.amount!.doubleValue
        } else {
            let budget = CoreDataHelper.sharedInstance.dataContext.budget.createEntity()
            budget.amount = 0
            budget.categoryId = typeId
        }

        budgetAmount = 10000
        total = budgetAmount - expenseTotal

        if budgetAmount != 0 {
            let percentage = (expenseTotal / budgetAmount)
            if percentage > 0 {
                if percentage < 1 {
                    cell.percentageRing.progress = 0.1
                }
                cell.percentageRing.progress = percentage
            } else {
                cell.percentageRing.progress = 0
            }
        } else {
            cell.percentageRing.progress = 0
        }

        cell.setCategoryColor(typeItems[indexPath.row].typeColor!.toColor())
        cell.totalAmountLabel.text = String(total).formatCurrencyStyle
        cell.budgetAmountLabel?.text = String(budgetAmount).formatCurrencyStyle
        cell.typeLabel?.text = typeItems[indexPath.row].name
        cell.expenseAmountLabel?.text = String(expenseTotal).formatCurrencyStyle
        cell.selectedBackgroundView = darkGrayBackgroundView
        return cell
    }
}
