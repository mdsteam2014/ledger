//
//  ItemTableViewHeaderFooterView.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/19.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

class ItemTableViewHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var dateLabel: UILabel!

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
