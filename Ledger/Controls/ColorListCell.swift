//
//  ColorListCell.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/28.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

final class ColorListCell: FormCell, KTColorPickerDelegate {

    // MARK: Public

    @IBOutlet weak var colorPicker: KTColorPicker!
    
    var colors = [UIColor]()
    var onColorSelected: (UIColor -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        configure()
          self.backgroundColor = CustomColors.addItemCellBackgroundColor
    }

    override func setup() {
        super.setup()
    }

    private func configure() {
        selectionStyle = .None
        colorPicker.delegate = self
    }

    func KTColorColorPickerTouched(sender:KTColorPicker, color:UIColor, point:CGPoint, state:UIGestureRecognizerState) {

        onColorSelected?(color)
    }
}
