//
//  TypeCollectionViewCell.swift
//  Ledger
//
//  Created by Alex Chen on 2016/5/27.
//  Copyright © 2016 Alex.Chen. All rights reserved.
//

import UIKit

class TypeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var view: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        self.view.layer.cornerRadius = 20
        self.view.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.view.layer.borderWidth = 1
        self.nameText.textColor = UIColor.lightGrayColor()
    }

}
