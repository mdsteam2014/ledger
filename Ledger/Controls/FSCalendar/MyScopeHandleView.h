//
//  ScopeHandleView.h
//  Ledger
//
//  Created by Alex Chen on 2016/6/11.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyScopeHandleView : UIView

@property (assign, nonatomic) BOOL isChange;

- (void)change;

@end
