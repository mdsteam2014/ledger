//
//  ScopeHandleView.m
//  Ledger
//
//  Created by Alex Chen on 2016/6/11.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

#import "MyScopeHandleView.h"


@implementation MyScopeHandleView



- (void) change {

    if(self.isChange) {
        self.isChange = NO;
    } else {
        self.isChange = YES;
    }
    [self setNeedsDisplay];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    UIBezierPath* aPath = [UIBezierPath bezierPath];

    aPath.lineWidth = 5.5;

    aPath.lineJoinStyle = kCGLineCapRound;


    if (self.isChange) {


        [aPath moveToPoint:CGPointMake(6.5, 14.5)];
        [aPath addLineToPoint:CGPointMake(24.5, 14.5)];
        [aPath addLineToPoint:CGPointMake(35, 14.5)];
        [aPath addLineToPoint:CGPointMake(24.5, 14.5)];
        [aPath addLineToPoint:CGPointMake(6.5, 14.5)];
    } else {

        [aPath moveToPoint:CGPointMake(8.5, 10.5)];
        [aPath addLineToPoint:CGPointMake(22.5, 6.5)];
        [aPath addLineToPoint:CGPointMake(36, 10.5)];
        [aPath addLineToPoint:CGPointMake(22.5, 6.5)];
        [aPath addLineToPoint:CGPointMake(8.5, 10.5)];
    }

    aPath.lineCapStyle = kCGLineCapRound;

    UIColor *fillColor = [UIColor grayColor];
    [fillColor setFill];
    UIColor *strokeColor = [UIColor grayColor];
    [strokeColor setStroke];
    [aPath closePath];
    [aPath fill];
    [aPath stroke];
}



@end
