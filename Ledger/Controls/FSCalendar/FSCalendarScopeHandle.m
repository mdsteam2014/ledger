//
//  FSCalendarScopeHandle.m
//  FSCalendar
//
//  Created by dingwenchao on 4/29/16.
//  Copyright © 2016 wenchaoios. All rights reserved.
//

#import "FSCalendarScopeHandle.h"
#import "FSCalendar.h"
#import "FSCalendarAnimator.h"
#import "UIView+FSExtension.h"
#import "MyScopeHandleView.h"

@interface FSCalendarScopeHandle () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) UIView *topBorder;
@property (weak, nonatomic) UIView *handleIndicator;

@property (weak, nonatomic) FSCalendarAppearance *appearance;

@property (assign, nonatomic) CGFloat lastTranslation;

- (void)handlePan:(id)sender;

@end

@implementation FSCalendarScopeHandle

MyScopeHandleView *scopeView;

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *view;
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 1)];
        view.backgroundColor = FSCalendarStandardSeparatorColor;
        self.topBorder = view;


        scopeView = [[MyScopeHandleView alloc] initWithFrame:CGRectMake(0, 0, 42, 30)];
        scopeView.backgroundColor = [UIColor clearColor];
     
        [self addSubview:scopeView];
        self.handleIndicator = scopeView;

        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];

        UITapGestureRecognizer *dtapGestureRecognize = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];

        dtapGestureRecognize.delegate = self;
        panGesture.minimumNumberOfTouches = 1;
        panGesture.maximumNumberOfTouches = 2;
        panGesture.delegate = self;
        [self addGestureRecognizer:panGesture];
        [self addGestureRecognizer:dtapGestureRecognize];
        self.panGesture = panGesture;

    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.topBorder.frame = CGRectMake(0, 0, self.fs_width, 1);
    self.handleIndicator.center = CGPointMake(self.fs_width/2, self.fs_height/2-0.5);
}

#pragma mark - Target actions

- (void)handleTap:(id)sender
{
    [self.delegate scopeHandleDidTap:self];
    [scopeView change];
}


- (void)change
{
    [scopeView change];
}

- (void)handlePan:(id)sender
{

    switch (self.panGesture.state) {
        case UIGestureRecognizerStateBegan: {
            if (self.delegate && [self.delegate respondsToSelector:@selector(scopeHandleDidBegin:)]) {
                [self.delegate scopeHandleDidBegin:self];
            }
            break;
        }
        case UIGestureRecognizerStateChanged: {
            if (self.delegate && [self.delegate respondsToSelector:@selector(scopeHandleDidUpdate:)]) {
                [self.delegate scopeHandleDidUpdate:self];
            }
            break;
        }
        case UIGestureRecognizerStateEnded: {
            if (self.delegate && [self.delegate respondsToSelector:@selector(scopeHandleDidEnd:)]) {
                [self.delegate scopeHandleDidEnd:self];
            }
            break;
        }
        case UIGestureRecognizerStateCancelled: {
            if (self.delegate && [self.delegate respondsToSelector:@selector(scopeHandleDidEnd:)]) {
                [self.delegate scopeHandleDidEnd:self];
            }
            break;
        }
        case UIGestureRecognizerStateFailed: {
            if (self.delegate && [self.delegate respondsToSelector:@selector(scopeHandleDidEnd:)]) {
                [self.delegate scopeHandleDidEnd:self];
            }
            break;
        }
        default: {
            break;
        }
    }
}

@end
