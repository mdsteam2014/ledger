//
//  CalculatorView.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/29.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation
import UIKit

protocol CalculatorViewDelegate {
    func calculatorKeyDown(number: String)
    func calculatorDone()
}




class CalculatorView: UIView {

    var delegate: CalculatorViewDelegate?
    internal var processor = CalculatorProcessor()
    @IBOutlet weak var acBtn: UIButton!
    @IBOutlet weak var divisionBtn: UIButton!
    @IBOutlet weak var multiplBtn: UIButton!
    @IBOutlet weak var SubtractionBtn: UIButton!
    @IBOutlet weak var additionBtn: UIButton!

    var textFieldInputView : UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "CalculatorView", bundle: bundle)
        let container = nib.instantiateWithOwner(self, options: nil)[0] as? UIView
        container?.backgroundColor = UIColor.clearColor()
        return container!
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
    }

    func resetCalculationButtonColor() {
        self.divisionBtn.backgroundColor = CustomColors.CalculatorOriginColor
        self.multiplBtn.backgroundColor = CustomColors.CalculatorOriginColor
        self.SubtractionBtn.backgroundColor = CustomColors.CalculatorOriginColor
        self.additionBtn.backgroundColor = CustomColors.CalculatorOriginColor
    }

    @IBAction func DownTouchDown(sender: AnyObject) {
        self.delegate?.calculatorDone()
    }

    @IBAction func buttonTouchDown(sender: AnyObject) {
        var output = ""

        resetCalculationButtonColor()
        switch (sender.tag) {
        case (CalculatorKey.Zero.rawValue)...(CalculatorKey.Nine.rawValue):
            output = processor.storeOperand(sender.tag-1)
        case CalculatorKey.Decimal.rawValue:
            output = processor.addDecimal()
        case CalculatorKey.Clear.rawValue:
            output = processor.clearAll()
        case CalculatorKey.Delete.rawValue:
            output = processor.deleteLastDigit()
        case (CalculatorKey.Multiply.rawValue)...(CalculatorKey.Add.rawValue):
            if let btn = sender as? UIButton {
                btn.backgroundColor = CustomColors.CalculatorDownColor
            }
            output = processor.storeOperator(sender.tag)
        case CalculatorKey.Equal.rawValue:
            resetCalculationButtonColor()
            output = processor.computeFinalValue()
            break
        default:
            break
        }
        delegate?.calculatorKeyDown(output)
    }
}