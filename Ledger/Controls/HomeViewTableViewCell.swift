//
//  HomeViewTableViewCell.swift
//  Ledger
//
//  Created by Alex Chen on 2016/5/29.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation


public class HomeViewTableViewCell : UITableViewCell {


    @IBOutlet weak var leftButton: KTButton!
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!

    @IBAction func imageButtonTouchDown(sender: AnyObject) {

    }

    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clearColor()
        self.selectedBackgroundView = bgColorView
    }
}