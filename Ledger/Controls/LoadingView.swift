//
//  LoadingView.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/16.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation

class LoadingView: UIView {

    var label: UILabel? = nil
    var activityIndicatorView: DGActivityIndicatorView?

    func showIndicator(uiView: UIView, month: Int) {
        self.frame = CGRectMake(0, 0, uiView.frame.size.width, uiView.frame.size.height)
        activityIndicatorView = DGActivityIndicatorView(type: DGActivityIndicatorAnimationType.DoubleBounce, tintColor: UIColor.whiteColor())
        let size: CGFloat = 50.0
        activityIndicatorView!.frame = CGRectMake((uiView.frame.size.width / 2) - (size / 2), (uiView.frame.size.height / 2) - (size / 2), size, size)
        activityIndicatorView!.startAnimating()
        self.addSubview(activityIndicatorView!)
        self.backgroundColor = CustomColors.alphaBackgroundColor

        label = UILabel(frame: CGRectMake(0, (uiView.frame.size.height / 2) + 10, uiView.frame.size.width, size))
        label!.textColor = UIColor.whiteColor()
        label!.text = "讀取\(month)月份電子發票資訊"
        label!.textAlignment = NSTextAlignment.Center
        label!.font = UIFont(name: label!.font.fontName, size: 12)
        self.addSubview(label!)
        uiView.addSubview(self)
    }

    func hideIndicator(uiView: UIView) {

        self.label?.removeFromSuperview()
        self.activityIndicatorView?.removeFromSuperview()
        self.removeFromSuperview()
    }
}