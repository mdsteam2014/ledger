//
//  KTPageControl.swift
//  Ledger
//
//  Created by Alex Chen on 2016/5/27.
//  Copyright © 2016 Alex.Chen. All rights reserved.
//

import Foundation
import UIKit

class KTPageControl: UIViewController, UIScrollViewDelegate {

    let titleY: CGFloat = 26
    let pageControlY = 70

    var scrollView:UIScrollView!
    var pageControl:UIPageControl!
    var selectedPage = 0
    var titleLabels: [UILabel] = [UILabel]()

    func addView(views: [UIViewController], titles: [String]) {

        self.view.backgroundColor = CustomColors.backgroundColor

        //Creating some shorthand for these values
        let wBounds = self.view.bounds.width
        let hBounds = self.view.bounds.height

        // This houses all of the UIViews / content
        scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.clearColor()
        scrollView.frame = self.view.frame
        scrollView.pagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        scrollView.bounces = false
        self.view.addSubview(scrollView)

        self.scrollView.contentSize = CGSize(width: self.view.bounds.size.width * CGFloat(views.count), height: hBounds/2)

        let height = self.navigationController!.navigationBar.frame.size.height

        //Paging control is added to a subview in the uinavigationcontroller
        pageControl = UIPageControl()
        pageControl.frame = CGRect(x: self.view.frame.width / 2, y: height + 10, width: 0, height: 0)
        pageControl.backgroundColor = UIColor.whiteColor()
        pageControl.numberOfPages = views.count
        pageControl.currentPage = 0
        pageControl.currentPageIndicatorTintColor = CustomColors.homeAddButtonBackgroundColor
        pageControl.pageIndicatorTintColor = UIColor.grayColor()

        self.navigationController!.navigationBar.barTintColor = CustomColors.backgroundColor
        self.navigationController!.view.addSubview(pageControl)

        //Titles for the nav controller (also added to a subview in the uinavigationcontroller)
        //Setting size for the titles. FYI changing width will break the paging fades/movement
        let titleSize = CGRect(x: 0, y: -20, width: wBounds, height: 16)

        for titleString in titles {
            let title = UILabel()
            title.frame = titleSize
            title.text = titleString
            title.textAlignment = NSTextAlignment.Center
            title.alpha = 0
            title.textColor = UIColor.whiteColor()
            self.navigationController!.view.addSubview(title)
            titleLabels.append(title)
        }

        for (index, view) in views.enumerate() {
            //view.view.backgroundColor = UIColor(red:0.325, green:0.667, blue:0.922, alpha: 1)
            view.view.frame = CGRectMake(CGFloat(index) * wBounds, 0, wBounds, hBounds)
            self.scrollView.addSubview(view.view)
            self.scrollView.bringSubviewToFront(view.view)
        }

        (titleLabels.first)?.alpha = 1
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func scrollViewDidScroll(scrollView: UIScrollView) {

        let xOffset: CGFloat = scrollView.contentOffset.x

        //Setup some math to position the elements where we need them when the view is scrolled
        let wBounds = self.view.bounds.width
        let widthOffset = wBounds / 100
        let offsetPosition = 0 - xOffset/widthOffset

        //Apply the positioning values created above to the frame's position based on user's scroll

        for (index, title) in self.titleLabels.enumerate() {
            title.frame = CGRectMake(offsetPosition + (100 * CGFloat(index)), titleY, wBounds, 20)
        }

        let first = self.titleLabels.first
        let last = self.titleLabels.last

        first!.alpha = 1 - xOffset / wBounds

        if (xOffset <= wBounds) {
            last!.alpha = xOffset / wBounds
        } else {
            last!.alpha = 1 - (xOffset - wBounds) / wBounds
        }
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {

        let xOffset: CGFloat = scrollView.contentOffset.x

        if (xOffset < 1.0) {
            pageControl.currentPage = 0
        } else if (xOffset < self.view.bounds.width + 1) {
            pageControl.currentPage = 1
        } else {
            pageControl.currentPage = 2
        }
    }

}