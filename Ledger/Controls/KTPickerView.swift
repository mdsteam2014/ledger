//
//  KTPickerView.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/2.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation
import UIKit

public class KTPickerView: UIPickerView, UIGestureRecognizerDelegate {


    // MARK: Init
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupLayer()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayer()

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(KTPickerView.handleTap(_:)))

        gestureRecognizer.delegate = self
        self.addGestureRecognizer(gestureRecognizer)


    }

    // MARK: Setup
    private func setupLayer() {
        self.layer.borderColor = UIColor.whiteColor().CGColor
        self.layer.borderWidth = 1.5
        self.layer.cornerRadius = 15
    }

    public func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 200)


        self.reloadAllComponents()
    }



}