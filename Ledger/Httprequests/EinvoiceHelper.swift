//
//  EinvoiceHelper.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/15.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation
import AlecrimCoreData

class EinvoiceHelper: NSObject {

    var dataContext: DataContext?

    class var sharedInstance: EinvoiceHelper {
        struct Singleton {
            static let instance = EinvoiceHelper()
        }
        return Singleton.instance
    }

    override init() {
        dataContext = DataContext(dataContextOptions: (UIApplication.sharedApplication().delegate as! AppDelegate).getContextOptions())
    }


    func getCarrierInvChk(date: NSDate, completionHandler: (Bool) -> Void) {

        let qs = QueryStringAppender()
        qs.add("version", aValue: "0.1")
        qs.add("cardType", aValue: "3J0002")
        qs.add("cardNo", aValue: "/QXSUFWR")
        qs.add("expTimeStamp", aValue: generateTimestamp())
        qs.add("action", aValue: "carrierInvChk")
        qs.add("timeStamp", aValue: generateTimestamp())
        qs.add("startDate", aValue: date.dateAtTheStartOfMonth().toString(format: .Custom("yyyy/MM/dd")))
        qs.add("endDate", aValue: date.dateAtTheEndOfMonth().toString(format: .Custom("yyyy/MM/dd")))
        qs.add("onlyWinningInv", aValue: "N")
        qs.add("appID", aValue: "EINV9201605288319")
        qs.add("uuid", aValue: "06BD81B17A829ED2E0643FDF69C2D0DF")
        qs.add("cardEncrypt", aValue: "0929283575")
        let baseUri = "https://www.einvoice.nat.gov.tw/PB2CAPIVAN/invServ/InvServ" + qs.getString()
        let request = BaseRequest(urlString: baseUri)
        request.sendAsynchronousRequest { (json) in

/*
            if let details = json!["details"].array {

                let type = self.dataContext?.expensesType.filter({$0.name == "悠遊卡消費"}).first
                let exchangeType = self.dataContext?.expensesType.filter({$0.name == "悠遊卡"}).first
                let bank = self.dataContext?.bankAccount.filter({$0.isDefault == true}).first

                for detail in details {

                    print(detail)
                    
                    let invNum = detail["invNum"].stringValue
                    let item = self.dataContext?.einvoice.filter({ $0.invNum == invNum }).first
                    if item == nil {

                        let expense = self.dataContext?.expense.createEntity()
                        let einvoice = self.dataContext?.einvoice.createEntity()

                        let date = detail["invDate"]["date"].stringValue
                        var month = detail["invDate"]["month"].stringValue
                        let year = detail["invDate"]["year"].intValue + 1911

                        if month.characters.count == 1 {
                            month = "0\(month)"
                        }
                        let dateString = "\(year)-\(month)-\(date)"

                        einvoice!.invNum = invNum
                        einvoice!.sellerName = detail["sellerName"].stringValue
                        einvoice!.cardType = detail["cardType"].stringValue
                        einvoice!.invStatus = detail["invStatus"].intValue
                        einvoice!.cardNo = detail["cardNo"].intValue
                        einvoice!.amount = detail["amount"].double
                        einvoice!.invPeriod = detail["invPeriod"].stringValue
                        einvoice!.isUpdate = false
                        einvoice!.invDate = NSDate(fromString: dateString, format: .Custom("yyyy-MM-dd"))
                        einvoice!.invDonatable = detail["invDate"].boolValue

                        expense?.amount = detail["amount"].double
                        expense?.time = NSDate(fromString: dateString, format: .Custom("yyyy-MM-dd"))
                        expense?.einvoiceNum = invNum
                        expense?.bankAccount = bank
                        expense?.expenseType = type
                    }
                }

                do {
                    try self.dataContext!.save()
                }
                catch {
                    print(error)
                }
            }
*/
            completionHandler(true)
        }
    }

    func getCarrierInvDetail(invNum: String, invNumDate: NSDate, completionHandler: (Bool) -> Void) {
        let qs = QueryStringAppender()
        qs.add("version", aValue: "0.1")
        qs.add("cardType", aValue: "3J0002")
        qs.add("cardNo", aValue: "/QXSUFWR")
        qs.add("expTimeStamp", aValue: generateTimestamp())
        qs.add("action", aValue: "carrierInvDetail")
        qs.add("timeStamp", aValue: generateTimestamp())
        qs.add("invNum", aValue: invNum)
        qs.add("invDate", aValue: invNumDate.toString(format: .Custom("yyyy/MM/dd")))
        qs.add("uuid", aValue: "06BD81B17A829ED2E0643FDF69C2D0DF")
        qs.add("appID", aValue: "EINV9201605288319")
        qs.add("cardEncrypt", aValue: "0929283575")
        let baseUri = "https://www.einvoice.nat.gov.tw/PB2CAPIVAN/invServ/InvServ" + qs.getString()
        let request = BaseRequest(urlString: baseUri)
        request.sendAsynchronousRequest { (json) in
            print("JSON: \(json)")
            completionHandler(true)
        }
    }

    private func generateTimestamp() -> String{
        let nowDouble = (NSDate().timeIntervalSince1970) * 1000
        return String(format:"%.0f", nowDouble)
    }


}