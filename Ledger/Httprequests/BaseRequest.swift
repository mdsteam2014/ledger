//
//  BaseRequest.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/15.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation

class BaseRequest: NSObject {

    let urlString: String!

    init(urlString: String) {
        self.urlString = urlString
    }

    func sendAsynchronousRequest(completionHandler: (JSON?) -> Void){
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: NSURL(string:  self.urlString)!)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            if(error == nil) {
                let json = JSON(data: data!)
                dispatch_async(dispatch_get_main_queue()) {
                    completionHandler(json)
                }
            }
        })
        task.resume()
    }
}
