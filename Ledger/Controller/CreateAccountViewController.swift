//
//  CreateAccountViewController.swift
//  Ledger
//
//  Created by Alex Chen on 2016/5/26.
//  Copyright © 2016 Alex.Chen. All rights reserved.
//

import UIKit

class CreateAccountViewController: FormViewController, CalculatorViewDelegate {

    let createHeader: (() -> ViewFormer) = {
        return CustomViewFormer<FormHeaderFooterView>()
            .configure {
                $0.viewHeight = 20
        }
    }

    @IBOutlet weak var addButton: UIBarButtonItem!
    var amountRow: TextFieldRowFormer<FormTextFieldCell>?
    let calculatorView = CalculatorView()
    var selectedIndex: Int?
    var isEditMode = false
    var bankAccount: BankAccount?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addButton.enabled = isEditMode
        self.calculatorView.delegate = self
        configure()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(animated: Bool) {

        for parent in self.navigationController!.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background")!, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        self.view.backgroundColor = CustomColors.backgroundColor
    }

    private func configure() {
        if !self.isEditMode {
            bankAccount = CoreDataHelper.sharedInstance.dataContext.bankAccount.createEntity()
            bankAccount!.amount = 0
            bankAccount!.id = NSUUID().UUIDString
            bankAccount!.index = CoreDataHelper.sharedInstance.dataContext.bankAccount.count() - 1

            bankAccount!.accountColor = UIColor().getRandomColor().toData()
        } else {
            bankAccount = CoreDataHelper.sharedInstance.dataContext.bankAccount.filter({ $0.index == self.selectedIndex }).first
        }

        tableView.contentInset.bottom = 30
        tableView.contentOffset.y = -10
        tableView.backgroundColor = CustomColors.backgroundColor
        tableView.separatorColor = CustomColors.itemSeparatorBackgroundColor

        amountRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "餘額"
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.textField.textColor = UIColor.whiteColor()
            $0.textField.font = .systemFontOfSize(15)

            $0.textField.inputView = self.calculatorView.textFieldInputView
            }.configure {
                $0.text = self.bankAccount?.amount?.stringValue.formatCurrencyStyle
                self.calculatorView.processor.storeOperand((self.bankAccount?.amount?.integerValue)!)
                $0.attributedPlaceholder = NSAttributedString(string:"請輸入金額",
                    attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
        }

        let bankNameRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "帳戶名稱"
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.textField.textColor = UIColor.whiteColor()
            $0.textField.font = .systemFontOfSize(15)
            }.configure {
                $0.text = self.bankAccount?.bankName
                $0.attributedPlaceholder = NSAttributedString(string:"請輸入帳戶名稱",
                    attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
            }.onTextChanged {
                self.bankAccount?.bankName = $0
                if (self.bankAccount?.bankName!.characters.count == 0) {
                    self.addButton.enabled = false
                } else {
                    self.addButton.enabled = true
                }
        }


        let colorSelectRow = CustomRowFormer<SelectColorCell>(instantiateType: .Nib(nibName: "SelectColorCell")) {
            $0.setColor(self.bankAccount!.accountColor!.toColor())
        }

        let colorListRow = CustomRowFormer<ColorListCell>(instantiateType: .Nib(nibName: "ColorListCell")) {
            $0.colorPicker.setColor(self.bankAccount!.accountColor!.toColor())
            $0.onColorSelected = { color in
                self.bankAccount!.accountColor = color.toData()
                colorSelectRow.cellUpdate({
                    $0.colorView.backgroundColor = color
                })
            }
        }

        let titleSection = SectionFormer(rowFormer: bankNameRow, amountRow!, colorSelectRow, colorSelectRow, colorListRow)
            .set(headerViewFormer: createHeader())

        former.append(sectionFormer: titleSection)
    }

    // MARK: - Calculator delegate

    func calculatorKeyDown(number: String) {
        self.amountRow?.cell.textField.text = number.formatCurrencyStyle
        self.bankAccount?.amount = Double(number)
    }

    func calculatorDone() {
        self.amountRow?.cell.textField.resignFirstResponder()
    }
}
