

import UIKit

class TypeSelectorViewContoller: FormViewController {

    // MARK: Public

    private var height : CGFloat = 0
    var setHeight : CGFloat {
        set { height = newValue }
        get { return height }
    }

    var type = Int() {
        didSet {
            reloadForm()
        }
    }

    var selectedText: String? {
        didSet {
            former.rowFormers.forEach {
                if let LabelRowFormer = $0 as? LabelRowFormer<FormLabelCell>
                    where LabelRowFormer.text == selectedText {
                }
            }
        }
    }

    var rowFormer: [LabelRowFormer<FormLabelCell>]?
    var categoryTypes: [CategoryType]?
    var onSelected: (CategoryType -> Void)?
    var textField: UITextField!
    var controller: SubTypeSelectorViewContoller?
    let addStatusBar = UIView()
    let editBtn = UIButton(type: UIButtonType.System)
    let backBtn = UIButton(type: UIButtonType.System)
    let addBtn = UIButton(type: UIButtonType.System)


    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: CustomColors.tabBarTintColor], forState: UIControlState.Normal)



        self.navigationItem.rightBarButtonItem = UIBarButtonItem()
    }

    func editCategory(sender:UIButton!) {

        if tableView.editing {
            self.addBtn.removeFromSuperview()
            self.addStatusBar.addSubview(self.backBtn)
        } else {
            self.backBtn.removeFromSuperview()
            self.addStatusBar.addSubview(self.addBtn)
        }

        let title = tableView.editing ? "編輯" : "完成"
        editBtn.setTitle(title, forState: UIControlState.Normal)
        tableView.setEditing(!tableView.editing, animated: true)
    }

    func backButtonTouchDown(sender:UIButton!) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    func configurationTextField(textField: UITextField!) {
        textField.placeholder = "請輸入類別名稱"
        self.textField = textField
    }

    func addButtonTouchDown(sender:UIButton!) {
        let alert = UIAlertController(title: "新增類別", message: "", preferredStyle: UIAlertControllerStyle.Alert)

        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "取消", style: UIAlertActionStyle.Cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "確定", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in

            let rootType = CoreDataHelper.sharedInstance.dataContext.categoryType.createEntity()
            rootType.id = NSUUID().UUIDString
            rootType.name = self.textField.text
            rootType.ledgerType = 0
            rootType.isRoot = true
            rootType.index =  CoreDataHelper.sharedInstance.dataContext.categoryType.count() - 1
            rootType.hasSubCategory = false
            rootType.subCategoryCount = 0
            do {
                try CoreDataHelper.sharedInstance.dataContext.save()
            } catch {
                print(error)
            }

            self.reloadRow()

        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    func createLabelCell() -> [LabelRowFormer<FormLabelCell>] {
        return categoryTypes!.map { type -> LabelRowFormer<FormLabelCell> in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = UIColor.whiteColor()
                $0.titleLabel.font = .boldSystemFontOfSize(16)

                }.configure {
                    $0.text = type.name
                    $0.subText = "\(type.subCategoryCount!)"

                }.onSelected { [weak self] _ in

                    if type.subCategoryCount == 0 {
                        self?.onSelected?(type)
                        self?.navigationController?.popViewControllerAnimated(true)
                    } else {
                        self!.pushSubTypeSelectorViewContoller(type)
                    }
            }
        }
    }

    private func pushSubTypeSelectorViewContoller(type: CategoryType) {
        self.controller = SubTypeSelectorViewContoller()
        self.controller!.setHeight = self.height

        if type.ledgerType == 0 {
            self.controller!.title = "請選擇支出子類別"
        } else {
            self.controller!.title = "請選擇收入子類別"
        }
        self.controller!.rootTypeId = type.id!
        self.controller!.onSelected = {
            self.onSelected?($0)
        }
        self.former.deselect(true)
        self.navigationController?.pushViewController(self.controller!, animated: true)
    }

    private func reloadRow() {

        self.categoryTypes?.removeAll()

        if self.type == 0 {
            self.categoryTypes = CoreDataHelper.sharedInstance.dataContext.categoryType.toArray().filter({ $0.ledgerType == 0 && $0.isRoot == true })
            self.categoryTypes!.sortInPlace({ $0.index! < $1.index! })
        } else {
            self.categoryTypes = CoreDataHelper.sharedInstance.dataContext.categoryType.toArray().filter({ $0.ledgerType == 1 && $0.isRoot == true })
            self.categoryTypes!.sortInPlace({ $0.index! < $1.index! })
        }

        self.rowFormer = self.createLabelCell()
        let sectionFormer = SectionFormer(rowFormers: self.rowFormer!)
        self.former.removeAll().append(sectionFormer: sectionFormer).reload()
    }

    private func reloadForm() {
        tableView.contentInset.bottom = 30
        tableView.contentOffset.y = -10
        tableView.backgroundColor = CustomColors.backgroundColor
        tableView.separatorColor = CustomColors.itemSeparatorBackgroundColor


        former.canEditRow = true
        former.isShowTheAddSubItem = true
        former.onDeletedItem = {

            var categoryTypes = CoreDataHelper.sharedInstance.dataContext.categoryType.filter({ $0.ledgerType == 0 && $0.isRoot == true })
            categoryTypes.sortInPlace({ $0.index! < $1.index! })
            CoreDataHelper.sharedInstance.dataContext.categoryType.deleteEntity(categoryTypes[$0.row])
            do {
                try CoreDataHelper.sharedInstance.dataContext.save()
            } catch {
                print(error)
            }

            self.reloadRow()
        }

        former.onMoveItem = {

            var categoryTypes = CoreDataHelper.sharedInstance.dataContext.categoryType.filter({ $0.ledgerType == 0 && $0.isRoot == true })
            categoryTypes.sortInPlace({ $0.index! < $1.index! })
            let movedObject = categoryTypes[$0.row]
            categoryTypes.removeAtIndex($0.row)
            categoryTypes.insert(movedObject, atIndex: $1.row)
            for (i, item) in categoryTypes.enumerate() {
                item.index = i
            }
            do {
                try CoreDataHelper.sharedInstance.dataContext.save()
            } catch {
                print(error)
            }
            self.reloadRow()
        }

        former.onAddSubItem = {
            var categoryTypes = CoreDataHelper.sharedInstance.dataContext.categoryType.filter({ $0.ledgerType == 0 && $0.isRoot == true })
            categoryTypes.sortInPlace({ $0.index! < $1.index! })
            self.former.deselect(true)

            self.pushSubTypeSelectorViewContoller(categoryTypes[$0.row])
        }
        reloadRow()
    }
}