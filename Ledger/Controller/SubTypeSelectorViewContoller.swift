//
//  SubTypeSelectorViewContoller.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/5.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation

class SubTypeSelectorViewContoller: FormViewController {

    // MARK: Public

    private var height : CGFloat = 0
    var setHeight : CGFloat {
        set { height = newValue }
        get { return height }
    }

    var rootTypeId = String() {
        didSet {
            reloadForm()
        }
    }

    var selectedText: String? {
        didSet {
            former.rowFormers.forEach {
                if let LabelRowFormer = $0 as? LabelRowFormer<FormLabelCell>
                    where LabelRowFormer.text == selectedText {
                }
            }
        }
    }

    var rowFormer: [LabelRowFormer<FormLabelCell>]?
    var categoryTypes: [CategoryType]?
    var onSelected: (CategoryType -> Void)?
    var textField: UITextField!
    let addStatusBar = UIView()
    let editBtn = UIButton(type: UIButtonType.System)
    let backBtn = UIButton(type: UIButtonType.System)
    let addBtn = UIButton(type: UIButtonType.System)

    func editCategory(sender:UIButton!) {

        if tableView.editing {
            self.addBtn.removeFromSuperview()
            self.addStatusBar.addSubview(self.backBtn)
        } else {
            self.backBtn.removeFromSuperview()
            self.addStatusBar.addSubview(self.addBtn)
        }

        let title = tableView.editing ? "編輯" : "完成"
        editBtn.setTitle(title, forState: UIControlState.Normal)
        tableView.setEditing(!tableView.editing, animated: true)
    }

    func backButtonTouchDown(sender:UIButton!) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    func configurationTextField(textField: UITextField!) {
        textField.placeholder = "請輸入子類別名稱"
        self.textField = textField
    }

    func addButtonTouchDown(sender:UIButton!) {
        let alert = UIAlertController(title: "新增子類別", message: "", preferredStyle: UIAlertControllerStyle.Alert)

        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "取消", style: UIAlertActionStyle.Cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "確定", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in

            let categoryType = CoreDataHelper.sharedInstance.dataContext.categoryType.filter({ $0.id == self.rootTypeId }).first
            var count: Int = (categoryType?.subCategoryCount?.integerValue)!
            count = count + 1
            categoryType?.subCategoryCount = count

            let rootType = CoreDataHelper.sharedInstance.dataContext.categoryType.createEntity()
            rootType.id = NSUUID().UUIDString
            rootType.name = self.textField.text
            rootType.ledgerType = 0
            rootType.isRoot = false
            rootType.index =  CoreDataHelper.sharedInstance.dataContext.categoryType.count() - 1
            rootType.hasSubCategory = false
            rootType.subCategoryCount = 0
            rootType.rootId = self.rootTypeId
            do {
                try CoreDataHelper.sharedInstance.dataContext.save()
            } catch {
                print(error)
            }

            self.reloadRow()
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    func createLabelCell() -> [LabelRowFormer<FormLabelCell>] {
        return categoryTypes!.map { type -> LabelRowFormer<FormLabelCell> in
            return LabelRowFormer<FormLabelCell>() {

                $0.titleLabel.textColor = UIColor.whiteColor()
                $0.titleLabel.font = .boldSystemFontOfSize(16)

                }.configure {
                    $0.text = type.name
                }.onSelected { [weak self] _ in
                    self?.onSelected?(type)
                    self?.navigationController?.popToViewController((self?.navigationController?.viewControllers[0])!, animated: true)
            }
        }
    }

    private func reloadRow() {
        self.categoryTypes?.removeAll()

        self.categoryTypes = CoreDataHelper.sharedInstance.dataContext.categoryType.filter({ $0.rootId == rootTypeId })
        self.categoryTypes!.sortInPlace({ $0.index! < $1.index! })

        self.rowFormer = self.createLabelCell()

        // Create SectionFormers
        let sectionFormer = SectionFormer(rowFormers: rowFormer!)
        former.removeAll().append(sectionFormer: sectionFormer).reload()
    }

    private func reloadForm() {

        editBtn.frame = CGRectMake(UIScreen.mainScreen().bounds.width - 50, height / 2, 50, 32)
        editBtn.backgroundColor = UIColor.clearColor()
        editBtn.setTitle("編輯", forState: UIControlState.Normal)
        editBtn.tintColor = UIColor.whiteColor()
        editBtn.addTarget(self, action: #selector(self.editCategory(_:)), forControlEvents: UIControlEvents.TouchUpInside)

        backBtn.frame = CGRectMake(0, height / 2, 50, 32)
        backBtn.backgroundColor = UIColor.clearColor()
        backBtn.setTitle("返回", forState: UIControlState.Normal)
        backBtn.tintColor = UIColor.whiteColor()
        backBtn.addTarget(self, action: #selector(self.backButtonTouchDown(_:)), forControlEvents: UIControlEvents.TouchUpInside)

        addBtn.frame = CGRectMake(0, height / 2, 50, 32)
        addBtn.backgroundColor = UIColor.clearColor()
        addBtn.setTitle("新增", forState: UIControlState.Normal)
        addBtn.tintColor = UIColor.whiteColor()
        addBtn.addTarget(self, action: #selector(self.addButtonTouchDown(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        addStatusBar.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, height)
        tableView.contentInset.top = height
        addStatusBar.alpha = 0.8
        addStatusBar.layer.borderWidth = 0.5
        addStatusBar.layer.borderColor = CustomColors.itemSeparatorBackgroundColor.CGColor
        addStatusBar.backgroundColor = CustomColors.backgroundColor

        let title = UILabel()
        title.frame = CGRectMake(0, height / 2, UIScreen.mainScreen().bounds.width,30)
        title.text = "\(self.title!)"
        title.textColor = UIColor.whiteColor()
        title.textAlignment = .Center
        title.font = UIFont(name: "CaviarDreams", size: 20.0)
        self.addStatusBar.addSubview(title)
        self.addStatusBar.addSubview(editBtn)
        self.addStatusBar.addSubview(backBtn)
        self.view.addSubview(addStatusBar)

        tableView.contentInset.bottom = 0
        tableView.contentOffset.y = -10
        tableView.backgroundColor = CustomColors.backgroundColor
        tableView.separatorColor = CustomColors.itemSeparatorBackgroundColor

        former.canEditRow = true
        former.onDeletedItem = {

            var categoryTypes = CoreDataHelper.sharedInstance.dataContext.categoryType.filter({ $0.rootId == self.rootTypeId })
            categoryTypes.sortInPlace({ $0.index! < $1.index! })
            CoreDataHelper.sharedInstance.dataContext.categoryType.deleteEntity(categoryTypes[$0.row])
            do {
                try CoreDataHelper.sharedInstance.dataContext.save()
            } catch {
                print(error)
            }

            self.reloadRow()
        }

        former.onMoveItem = {
            var categoryTypes = CoreDataHelper.sharedInstance.dataContext.categoryType.filter({ $0.rootId == self.rootTypeId })
            categoryTypes.sortInPlace({ $0.index! < $1.index! })
            let movedObject = categoryTypes[$0.row]
            categoryTypes.removeAtIndex($0.row)
            categoryTypes.insert(movedObject, atIndex: $1.row)
            for (i, item) in categoryTypes.enumerate() {
                item.index = i
            }
            do {
                try CoreDataHelper.sharedInstance.dataContext.save()
            } catch {
                print(error)
            }
            self.reloadRow()
        }

        self.reloadRow()
    }
}