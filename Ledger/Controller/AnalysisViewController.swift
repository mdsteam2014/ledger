//
//  TestViewController.swift
//  Ledger
//
//  Created by Alex Chen on 2016/5/25.
//  Copyright © 2016 Alex.Chen. All rights reserved.
//

import UIKit
import Charts

class AnalysisViewController: UIViewController {

    @IBOutlet weak var segmentedControl: KTSegmentedControl!
    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var dateTextField: UITextField!
    var startDate: NSDate?
    var endDate: NSDate?
    var isEditDate = false
    var isStartEditDate = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.segmentedControl.items = ["自訂", "1年", "1個月", "1周", "1天"]
        setChart()
    }

    func dismissPicker() {
        reloadData()
        view.endEditing(true)
        isEditDate = false
    }

    func nextPicker() {
        isStartEditDate = false
        view.endEditing(true)
        self.dateTextField.becomeFirstResponder()
    }

    func datePickerValueChanged(sender:UIDatePicker) {
        if isStartEditDate {
            startDate = sender.date
        } else {
            endDate = sender.date
        }

        UserDefaultsHelper.sharedInstance.setAnalysisCustomStartDate(startDate!)
        UserDefaultsHelper.sharedInstance.setAnalysisCustomEndDate(endDate!)

        dateTextField.text = "\(startDate!.toString(format: .Custom("yyyy/MM/dd"))) - \(endDate!.toString(format: .Custom("yyyy/MM/dd")))"
    }

    @IBAction func segmentedControlValueChanged(sender: AnyObject) {
        UserDefaultsHelper.sharedInstance.setAnalysisSegmentIndex(self.segmentedControl.selectedIndex)
        self.reloadData()
    }

    func reloadData() {
        self.dateTextField.text = getfilterDateString()
        self.pieChart.clear()
        self.pieChart.clearValues()
        self.pieChart.data = generateBarData()

        pieChart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
    }

    override func viewWillAppear(animated: Bool) {
        self.segmentedControl.selectedIndex =
            UserDefaultsHelper.sharedInstance.getAnalysisSegmentIndex()
        self.reloadData()

        for parent in self.navigationController!.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background")!, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        self.view.backgroundColor = CustomColors.backgroundColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getfilterDateString() -> String {

        self.dateTextField.enabled = false

        switch self.segmentedControl.selectedIndex {
        case 0:

            self.dateTextField.enabled = true
            isStartEditDate = true

            startDate = UserDefaultsHelper.sharedInstance.getAnalysisCustomStartDate()
            endDate = UserDefaultsHelper.sharedInstance.getAnalysisCustomEndDate()
        case 1:
            view.endEditing(true)
            startDate = NSDate(fromString: "\(NSDate().year())/01/01", format: .ISO8601(nil))
            endDate = NSDate(fromString: "\(NSDate().year())/12/31", format: .ISO8601(nil))
        case 2:
            view.endEditing(true)
            startDate = NSDate().dateAtTheStartOfMonth()
            endDate = NSDate().dateAtTheEndOfMonth()
        case 3:
            view.endEditing(true)
            startDate = NSDate().dateAtStartOfWeek()
            endDate = NSDate().dateAtEndOfWeek()
        case 4:
            view.endEditing(true)
            startDate = NSDate()
            endDate = NSDate()
        default: return ""
        }

        return "\(startDate!.toString(format: .Custom("yyyy/MM/dd"))) - \(endDate!.toString(format: .Custom("yyyy/MM/dd")))"
    }

    func setChart() {
        pieChart.infoTextColor = UIColor.whiteColor()
        pieChart.descriptionTextColor = UIColor.clearColor()
        pieChart.legend.position = .BelowChartLeft
        pieChart.transparentCircleColor = CustomColors.itemSeparatorBackgroundColor
        pieChart.rotationEnabled = false
        pieChart.legend.textColor = UIColor.whiteColor()
        pieChart.holeColor = UIColor.clearColor()
        pieChart.holeRadiusPercent = 0.6
    }

    func generateBarData() -> PieChartData {

        let items = CoreDataHelper.sharedInstance.getLedgerByDateRang(startDate!, endDate: endDate!, type: 0)
        let groupItems = items.groupBy { $0.categoryTypeRootId!}

        var dataEntries: [ChartDataEntry] = []

        var dataPoints: [String] = []

        var colors: [UIColor] = []

        for (index, item) in groupItems.enumerate() {

            let type = CoreDataHelper.sharedInstance.dataContext.categoryType.filter({$0.id == item.0}).first

            colors.append(type!.typeColor!.toColor())
            dataPoints.append(type!.name!)
            var amount = 0.0

            for itm in item.1 {
                amount = amount + itm.amount!.doubleValue
            }

            let dataEntry = ChartDataEntry(value: amount, xIndex: index)
            dataEntries.append(dataEntry)
        }

        let pieChartDataSet = PieChartDataSet(yVals: dataEntries, label: "")
        pieChartDataSet.colors = colors
        return PieChartData(xVals: dataPoints, dataSet: pieChartDataSet)
    }

    @IBAction func editDidBegin(sender: AnyObject) {
        isEditDate = true
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.backgroundColor = CustomColors.itemSeparatorBackgroundColor
        datePickerView.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        self.dateTextField.inputView = datePickerView

        let toolBar = UIToolbar()
        toolBar.barTintColor = CustomColors.backgroundColor
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor.whiteColor()
        toolBar.sizeToFit()
        var timeString = ""
        let rightutton = UIBarButtonItem(title: "確定", style: UIBarButtonItemStyle.Plain, target: self, action: nil)

        if self.isStartEditDate {
            rightutton.action = #selector(nextPicker)
            timeString = "開始時間"
            datePickerView.date = self.startDate!
        } else {
            datePickerView.date = self.endDate!
            rightutton.action = #selector(dismissPicker)
            timeString = "結束時間"
        }
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let label = UIBarButtonItem(title: timeString, style: UIBarButtonItemStyle.Plain, target: self, action: nil)
        label.enabled = false
        toolBar.setItems([ label, spaceButton, rightutton], animated: false)
        toolBar.userInteractionEnabled = true
        self.dateTextField.inputAccessoryView = toolBar
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)

        self.dateTextField.becomeFirstResponder()
    }
}
