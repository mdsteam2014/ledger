//
//  FirstViewController.swift
//  Ledger
//
//  Created by Alex Chen on 2016/4/6.
//  Copyright © 2016 Alex.Chen. All rights reserved.
//

import UIKit
import AlecrimCoreData

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FSCalendarDataSource, FSCalendarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerViewConstraint: NSLayoutConstraint!

    @IBOutlet weak var balancePercentage: MKRingProgressView!
    @IBOutlet weak var budgetPercentage: MKRingProgressView!
    var isWeekly = false
    var refreshControl: UIRefreshControl!
    let loadingView: LoadingView = LoadingView()
    var dataContext: DataContext!
    var selectedDate: NSDate = NSDate()
    var selectedLedger: Ledger?

    override func viewDidLoad() {
        super.viewDidLoad()

        dataContext = DataContext(dataContextOptions: (UIApplication.sharedApplication().delegate as! AppDelegate).getContextOptions())

        calendar.scrollDirection = .Vertical

        self.refreshControl = UIRefreshControl()
        self.refreshControl.tintColor = UIColor.clearColor()
        self.refreshControl?.addTarget(self, action: #selector(HomeViewController.onPullToFresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl!)
        self.tableView.allowsSelection = true
        self.tableView.separatorColor = CustomColors.itemSeparatorBackgroundColor
        self.tableView.tableFooterView = UIView()
        buildAddButton()
    }

    override func viewDidAppear(animated: Bool) {
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.0)
        budgetPercentage.progress = 0.83
        balancePercentage.progress = -0.4
        CATransaction.commit()
        setTableViewofHeight()
    }

    override func viewWillAppear(animated: Bool) {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.registerNib(UINib(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemTableViewCell")
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.calendar.reloadData()
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func buildAddButton() {

        let buttonSize: CGFloat = 50.0
        let buttonY = self.view.frame.size.height - (tabBarController?.tabBar.frame.size.height)! - buttonSize - 10
        let buttonX = self.view.frame.size.width - 60
        let addButton = KTButton(frame: CGRect(x: buttonX, y: buttonY, width: buttonSize, height: buttonSize))
        addButton.backgroundColor = CustomColors.homeAddButtonBackgroundColor
        addButton.layer.cornerRadius = buttonSize / 2
        addButton.layer.shadowColor = UIColor.blackColor().CGColor
        addButton.layer.shadowOffset = CGSizeMake(0.0, 3.0)
        addButton.layer.masksToBounds = false
        addButton.layer.shadowRadius = 1.0
        addButton.layer.shadowOpacity = 0.5
        addButton.setImage(UIImage(named: "add"), forState: UIControlState.Normal)
        addButton.addTarget(self, action: #selector(addExpenseTouchDown), forControlEvents: .TouchUpInside)
        self.view.addSubview(addButton)

    }

    func onPullToFresh() {
        self.loadingView.showIndicator(self.view, month: self.calendar.currentPage.month())
        EinvoiceHelper.sharedInstance.getCarrierInvChk(self.calendar.currentPage) { (isSuccess) in
            dispatch_async(dispatch_get_main_queue()) {
                self.refreshControl.endRefreshing()
                self.loadingView.hideIndicator(self.view)
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Calendar Delegate

    func calendar(calendar: FSCalendar, didSelectDate date: NSDate) {
        self.selectedDate = date
        self.tableView.reloadData()
    }

    func calendar(calendar: FSCalendar, hasEventForDate date: NSDate) -> Bool {

        if self.dataContext.ledger.filter({$0.date!.isEqualToDateIgnoringTime(date)}).count > 0 {
            return true
        }

        return false
    }

    func calendar(calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = CGRectGetHeight(bounds)
        self.view.layoutIfNeeded()
        setTableViewofHeight()
    }

    func setTableViewofHeight() {
        let calendarHeight = self.calendarHeightConstraint.constant
        let centerViewHeight = self.centerViewConstraint.constant
        let newHeight = self.view.frame.size.height - centerViewHeight - calendarHeight
        self.tableViewConstraint.constant = newHeight - 30
    }


    // MARK: - TableView Delegate

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = CoreDataHelper.sharedInstance.dataContext.ledger.filter({$0.date!.isEqualToDateIgnoringTime(self.selectedDate)})
        selectedLedger = item[indexPath.row]
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.performSegueWithIdentifier("HomePushToPreview", sender: self)
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.dataContext.ledger.filter({$0.date!.isEqualToDateIgnoringTime(self.selectedDate)}).count
        return count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("ItemTableViewCell", forIndexPath: indexPath) as! ItemTableViewCell
        cell.backgroundColor = UIColor.clearColor()

        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.darkGrayColor()
        cell.selectedBackgroundView = backgroundView

        let item = self.dataContext.ledger.filter({$0.date!.isEqualToDateIgnoringTime(self.selectedDate)})

        let categoryTypeId = item[indexPath.row].categoryTypeId
        let bankId = item[indexPath.row].bankId
        let exchangeTypeId = item[indexPath.row].exchangeTypeId

        let bank = self.dataContext.bankAccount.toArray().filter({
            $0.id == bankId
        }).first

        let category = self.dataContext.categoryType.toArray().filter({
            $0.id == categoryTypeId
        }).first

        let rootCategory = self.dataContext.categoryType.toArray().filter({
            $0.id == category?.rootId
        }).first

        let type = self.dataContext.exchangeType.toArray().filter({
            $0.id == exchangeTypeId
        }).first

        if item[indexPath.row].ledgerType == 0 {
            cell.amountLabel.text = "-\(String(item[indexPath.row].amount!).formatCurrencyStyle)"
        } else {
            cell.amountLabel.text = String(item[indexPath.row].amount!).formatCurrencyStyle
        }

        cell.bankLabel.text = bank?.bankName
        cell.exchangeLabel.text = type?.name
        cell.categoryLabel.text = category?.name
        cell.typeColorView.backgroundColor = rootCategory?.typeColor?.toColor()
        return cell
    }

    func scopeHandleDidTap() {
        if self.calendar.scope == .Month {
            self.calendar.scope = .Week
        } else {
            self.calendar.scope = .Month
        }
    }


    // MARK: - Segue

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        switch segue.destinationViewController {
        case is PreviewLegerViewController:
            (segue.destinationViewController as! PreviewLegerViewController).selectedLedger = self.selectedLedger
            break
        case is UINavigationController:

            let destinationNavigationController = segue.destinationViewController as? UINavigationController
            let targetController = destinationNavigationController!.topViewController as! CreateLedgerViewController
            targetController.currentDate = self.selectedDate

            break
        default:
            break
        }
    }

    @IBAction func addExpenseTouchDown(sender: AnyObject) {
        self.performSegueWithIdentifier("goToCreateLedger", sender: self)
    }

    @IBAction func cancelToCreateLedgerViewController(segue:UIStoryboardSegue) {
    }

    @IBAction func saveLedger(segue:UIStoryboardSegue) {
        if let createLedgerViewController = segue.sourceViewController as? CreateLedgerViewController {
            //add the new player to the players array
            if createLedgerViewController.ledger != nil {
                do {
                    try CoreDataHelper.sharedInstance.dataContext.save()
                } catch {
                    print(error)
                }
            }
        }
    }
}

