//
//  MainViewController.swift
//  Ledger
//
//  Created by Alex Chen on 2016/4/6.
//  Copyright © 2016 Alex.Chen. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()


        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: UIControlState.Selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: CustomColors.tabBarTintColor], forState: UIControlState.Normal)

        (self.tabBar.items! as [UITabBarItem])[0].image = UIImage(named: "tabbar_account.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        (self.tabBar.items! as [UITabBarItem])[1].image = UIImage(named: "tabbar_budget.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        (self.tabBar.items! as [UITabBarItem])[2].image = UIImage(named: "tabbar_home.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        (self.tabBar.items! as [UITabBarItem])[3].image = UIImage(named: "tabbar_analysis.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        (self.tabBar.items! as [UITabBarItem])[4].image = UIImage(named: "tabbar_setting.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)

        self.selectedIndex = 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func editButtonTouchDown(sender: AnyObject) {

        NSNotificationCenter.defaultCenter().postNotificationName(Identifier.changeCalender, object: nil)
    }
}
