//
//  AccountViewController.swift
//  Ledger
//
//  Created by Alex Chen on 2016/5/26.
//  Copyright © 2016 Alex.Chen. All rights reserved.
//

import UIKit
import Charts
import AlecrimCoreData

class AccountViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var combineChartView: CombinedChartView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    var selectedBankIndex: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.contentSize = CGSizeMake(self.tableView.frame.size.width, self.tableView.contentSize.height)

        self.tableView.separatorColor = CustomColors.itemSeparatorBackgroundColor
        self.tableView.allowsSelectionDuringEditing = true
        self.tableView.tableFooterView = UIView()
        buildAddButton()
    }

    override func viewWillAppear(animated: Bool) {

        setChartPie()
        self.tableView.reloadData()

        for parent in self.navigationController!.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background")!, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        self.view.backgroundColor = CustomColors.backgroundColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    func buildAddButton() {

        let buttonSize: CGFloat = 50.0
        let buttonY = self.view.frame.size.height - (tabBarController?.tabBar.frame.size.height)! - buttonSize - 10
        let buttonX = self.view.frame.size.width - 60
        let addButton = KTButton(frame: CGRect(x: buttonX, y: buttonY, width: buttonSize, height: buttonSize))
        addButton.backgroundColor = CustomColors.homeAddButtonBackgroundColor
        addButton.layer.cornerRadius = buttonSize / 2
        addButton.layer.shadowColor = UIColor.blackColor().CGColor
        addButton.layer.shadowOffset = CGSizeMake(0.0, 3.0)
        addButton.layer.masksToBounds = false
        addButton.layer.shadowRadius = 1.0
        addButton.layer.shadowOpacity = 0.5
        addButton.setImage(UIImage(named: "add"), forState: UIControlState.Normal)
        addButton.addTarget(self, action: #selector(addButtonTouchDown), forControlEvents: .TouchUpInside)
        self.view.addSubview(addButton)

    }

    // MARK: - calculator delegate

    func calculateExpense(bank: BankAccount) -> Double {
        let items = CoreDataHelper.sharedInstance.dataContext.ledger.filter({ $0.ledgerType == 0 && $0.bankId == bank.id})
        var amount = bank.amount!.doubleValue
        for item in items {
            amount -= item.amount!.doubleValue
        }
        return amount
    }

    func calculateIncome(bank: BankAccount) -> Double {
        let items = CoreDataHelper.sharedInstance.dataContext.ledger.filter({$0.ledgerType == 1 && $0.bankId == bank.id})
        var amount = bank.amount!.doubleValue
        for item in items {
            amount += item.amount!.doubleValue
        }
        return amount
    }

    // MARK: - tableView delegate

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CoreDataHelper.sharedInstance.dataContext.bankAccount.count()
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let bank = CoreDataHelper.sharedInstance.dataContext.bankAccount.filter({
            $0.index == indexPath.row
        }).first!
        let expense = calculateExpense(bank)
        let income = calculateIncome(bank)
        let balance = bank.amount!.doubleValue + income + expense


        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        cell.backgroundColor = UIColor.clearColor()

        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.darkGrayColor()
        cell.selectedBackgroundView = backgroundView
        cell.textLabel?.text = bank.bankName
        cell.textLabel?.textColor = bank.accountColor?.toColor()
        cell.detailTextLabel?.text = String(balance).formatCurrencyStyle

        return cell
    }

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {

        if indexPath.row == 0 {
            return false
        } else {
            return true
        }
    }

    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {

        var categoryTypes = CoreDataHelper.sharedInstance.dataContext.bankAccount.toArray()
        categoryTypes.sortInPlace({ $0.index! < $1.index! })
        let movedObject = categoryTypes[sourceIndexPath.row]
        categoryTypes.removeAtIndex(sourceIndexPath.row)
        categoryTypes.insert(movedObject, atIndex: destinationIndexPath.row)
        for (i, item) in categoryTypes.enumerate() {
            item.index = i
        }

        do {
            try CoreDataHelper.sharedInstance.dataContext.save()
            self.tableView.reloadData()
        } catch {
            print(error)
        }
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedBankIndex = indexPath.row
        self.performSegueWithIdentifier("goToCreateAccount", sender: self)
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }

    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {

        let moreRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "預設張戶", handler:{action, indexpath in

            var banks = CoreDataHelper.sharedInstance.dataContext.bankAccount.toArray()
            banks.sortInPlace({ $0.index! < $1.index! })
            let movedObject = banks[indexpath.row]
            banks.removeAtIndex(indexpath.row)
            banks.insert(movedObject, atIndex: 0)
            for (i, item) in banks.enumerate() {
                item.isDefault = i == 0
                item.index = i
            }

            do {
                try CoreDataHelper.sharedInstance.dataContext.save()
                self.tableView.reloadData()
            } catch {
                print(error)
            }
        });

        moreRowAction.backgroundColor = UIColor(red: 0.298, green: 0.851, blue: 0.3922, alpha: 1.0);

        let deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "刪除", handler:{action, indexpath in

            if let item = CoreDataHelper.sharedInstance.dataContext.bankAccount.filter({ $0.index == indexPath.row }).first {
                CoreDataHelper.sharedInstance.dataContext.bankAccount.deleteEntity(item)

                var banks = CoreDataHelper.sharedInstance.dataContext.bankAccount.toArray()
                banks.sortInPlace({ $0.index! < $1.index! })
                for (i, item) in banks.enumerate() {
                    item.index = i
                }
                do {
                    try CoreDataHelper.sharedInstance.dataContext.save()

                    if CoreDataHelper.sharedInstance.dataContext.bankAccount.count() == 1 {
                        self.tableView.setEditing(false, animated: true)
                        self.editButton.title = "編輯"
                        self.editButton.tag = 0
                    }
                    self.tableView.reloadData()
                } catch {
                    print(error)
                }
            }

        });
        return [deleteRowAction, moreRowAction]
    }

    //MARK: - Chart set up

    func setChartPie() {

        let months: [String] = ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"]

        var amounts: [Double] = []

        for index in 1...12 {

            let items = CoreDataHelper.sharedInstance.dataContext.ledger.filter { $0.ledgerType == 0 && $0.date!.month() == index }

            var amount = 0.0
            for item in items {
                amount += (item.amount?.doubleValue)!
            }
            amounts.append(amount)
        }

        combineChartView.noDataText = "Please provide data for the chart."
        let data: CombinedChartData = CombinedChartData(xVals: months)
        data.barData = generateBarData(months, values: amounts)
        data.lineData = generateLineData(months, values: amounts)
        combineChartView.data = data
        combineChartView.infoTextColor = UIColor.whiteColor()
        combineChartView.descriptionTextColor = UIColor.clearColor()

        combineChartView.drawGridBackgroundEnabled = false
        combineChartView.leftAxis.enabled = false
        combineChartView.legend.enabled = false
        combineChartView.rightAxis.enabled = false
        combineChartView.leftAxis.drawGridLinesEnabled = false
        combineChartView.rightAxis.drawGridLinesEnabled = false

        combineChartView.xAxis.labelTextColor = UIColor.whiteColor()
        combineChartView.xAxis.labelPosition = .Bottom
        combineChartView.xAxis.drawGridLinesEnabled = false
        combineChartView.setScaleEnabled(false)
        combineChartView.pinchZoomEnabled = false
        combineChartView.autoScaleMinMaxEnabled = true
        combineChartView.moveViewToX(CGFloat(NSDate().month() - 1))

        combineChartView.animate(xAxisDuration: 1.5, yAxisDuration: 2.0)
        combineChartView.highlightPerTapEnabled = false
        combineChartView.highlightPerDragEnabled = false
    }

    func generateBarData(months: [String], values: [Double]) -> BarChartData {

        var dataEntries: [BarChartDataEntry] = []

        for i in 0..<months.count {
            let dataEntry = BarChartDataEntry(value: values[i] * 2, xIndex: i)
            dataEntries.append(dataEntry)
        }

        let barChartSet: BarChartDataSet = BarChartDataSet(yVals: dataEntries, label: "收入")
        barChartSet.barShadowColor = UIColor.clearColor()
        barChartSet.setColor(UIColor(red: 231/255, green: 18/255, blue: 0/255, alpha: 1))
        barChartSet.valueTextColor = UIColor(red: 60/255, green: 220/255, blue: 78/255, alpha: 1)
        return BarChartData(xVals: months, dataSets: [barChartSet])
    }

    func generateLineData(months: [String], values: [Double]) -> LineChartData {

        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()

        for i in 0..<months.count {
            yVals1.append(ChartDataEntry(value: values[i], xIndex: i))
        }
        let lineChartSet = LineChartDataSet(yVals: yVals1, label: "支出")
        lineChartSet.setColor(UIColor(red: 240/255, green: 238/255, blue: 70/255, alpha: 1))
        lineChartSet.lineWidth = 1
        lineChartSet.circleHoleRadius = 0
        lineChartSet.circleRadius = 4
        lineChartSet.fillColor = UIColor(red: 240/255, green: 238/255, blue: 70/255, alpha: 1)
        lineChartSet.valueFont = NSUIFont.systemFontOfSize(10.0)
        lineChartSet.valueTextColor = UIColor(red: 240/255, green: 238/255, blue: 70/255, alpha: 1)

        lineChartSet.mode = LineChartDataSet.Mode.CubicBezier
        return LineChartData(xVals: months, dataSets: [lineChartSet])
    }

    //MARK - Prepare For Segue

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        if let view = segue.destinationViewController.childViewControllers[0] as? CreateAccountViewController {
            view.selectedIndex = self.selectedBankIndex
            view.isEditMode = self.tableView.editing
        }
    }

    @IBAction func editButtonAction(sender: AnyObject) {
        if (sender as! UIBarItem).tag == 0 {
            self.tableView.setEditing(true, animated: true)
            (sender as! UIBarItem).title = "完成"
            (sender as! UIBarItem).tag = 1
        } else {
            self.tableView.setEditing(false, animated: true)
            (sender as! UIBarItem).title = "編輯"
            (sender as! UIBarItem).tag = 0
        }
    }

    @IBAction func addButtonTouchDown(sender: AnyObject) {
        editButton.title = "編輯"
        editButton.tag = 0
        self.tableView.setEditing(false, animated: false)
        self.performSegueWithIdentifier("goToCreateAccount", sender: self)
    }

    @IBAction func saveAccount(segue:UIStoryboardSegue) {
        if let viewController = segue.sourceViewController as? CreateAccountViewController {
            //add the new player to the players array
            if viewController.bankAccount != nil {
                do {
                    try CoreDataHelper.sharedInstance.dataContext.save()
                } catch {
                    print(error)
                }
            }
        }
    }
    
    @IBAction func cancelToCreateAccountViewController(segue:UIStoryboardSegue) {
        CoreDataHelper.sharedInstance.dataContext.reset()
    }
}
