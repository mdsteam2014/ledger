//
//  BudgetDetailViewController.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/18.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit
import Charts

class BudgetDetailViewController: UIViewController, ChartViewDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var tableView: UITableView!

    var selectedCategoryType: CategoryType?
    var selectedDate: NSDate?
    var ledgerGroup: [[Int: [Ledger]]] = [[Int: [Ledger]]]()

    let selectedBackgroundView = UIView()
    var selectedLedger: Ledger?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.backgroundColor = CustomColors.backgroundColor
        self.tableView.separatorColor = CustomColors.itemSeparatorBackgroundColor
        self.tableView.registerNib(UINib(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemTableViewCell")
        self.tableView.sectionHeaderHeight = 16
        selectedBackgroundView.backgroundColor = UIColor.darkGrayColor()

        setChart()
    }

    // 找出當月份，並且group by
    func getLedgerOnlyMonth() {
        ledgerGroup.removeAll()
        if let day = self.selectedDate?.monthDays() {
            for index in 1..<day {
                let ledgers = self.getLedgers(index)
                var ls: [Ledger] = []
                for ledger in ledgers {
                    ls.append(ledger)
                }
                if ls.count > 0 {
                    let group: [Int: [Ledger]] = [index:ls]
                    ledgerGroup.append(group)
                }
            }
        }
        ledgerGroup = ledgerGroup.reverse()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        for parent in self.navigationController!.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background")!, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        self.view.backgroundColor = CustomColors.backgroundColor


        getLedgerOnlyMonth()
        barChartView.clear()
        barChartView.clearValues()
        barChartView.data = generateBarData()
        self.tableView.reloadData()
        barChartView.moveViewToX(CGFloat(NSDate().day() - 5))
    }

    override func viewWillDisappear(animated: Bool) {
        self.selectedBackgroundView.removeFromSuperview()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let view = segue.destinationViewController as? PreviewLegerViewController {
            view.selectedLedger = self.selectedLedger
        }
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)

        if let item = ledgerGroup[indexPath.section].values.first {
            self.selectedLedger = item[indexPath.row]
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            self.performSegueWithIdentifier("BudgetPushToPreview", sender: self)
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return ledgerGroup.count
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ledgerGroup[section].values.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("ItemTableViewCell", forIndexPath: indexPath) as! ItemTableViewCell
        cell.backgroundColor = UIColor.clearColor()

        cell.selectedBackgroundView = selectedBackgroundView

        if let item = ledgerGroup[indexPath.section].values.first {

            let categoryTypeId = item[indexPath.row].categoryTypeId
            let bankId = item[indexPath.row].bankId
            let exchangeTypeId = item[indexPath.row].exchangeTypeId

            let bank = CoreDataHelper.sharedInstance.dataContext.bankAccount.toArray().filter({
                $0.id == bankId
            }).first

            let category = CoreDataHelper.sharedInstance.dataContext.categoryType.toArray().filter({
                $0.id == categoryTypeId
            }).first

            let rootCategory = CoreDataHelper.sharedInstance.dataContext.categoryType.toArray().filter({
                $0.id == category?.rootId
            }).first

            let type = CoreDataHelper.sharedInstance.dataContext.exchangeType.toArray().filter({
                $0.id == exchangeTypeId
            }).first

            if let amount = item[indexPath.row].amount {
                if item[indexPath.row].ledgerType == 0 {
                    cell.amountLabel.text = "\(String(amount).formatCurrencyStyle)"
                } else {
                    cell.amountLabel.text = String(amount).formatCurrencyStyle
                }
            }

            cell.bankLabel.text = bank?.bankName
            cell.exchangeLabel.text = type?.name
            cell.categoryLabel.text = category?.name
            cell.typeColorView.backgroundColor = rootCategory?.typeColor?.toColor()
        }

        return cell
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView = UIView(frame: CGRectMake(0, 0, tableView.bounds.size.width, 16))
        headerView.backgroundColor = CustomColors.itemSeparatorBackgroundColor
        let dateString = self.ledgerGroup[section].keys.first
        let str = self.selectedDate!.toString(format: .Custom("yyyy/MM/"))
        let label = UILabel(frame: CGRectMake(5, 0, tableView.bounds.size.width, 16))
        label.text = "\(str)\(dateString!)"
        label.font = UIFont(name: "HelveticaNeue-light", size: 12)
        label.textColor = UIColor.whiteColor()
        headerView.addSubview(label)
        return headerView
    }

    func getLedgers(day: Int) -> [Ledger] {
        return CoreDataHelper.sharedInstance.dataContext.ledger.filter({
            $0.categoryTypeRootId == self.selectedCategoryType!.id
                && $0.date!.isEqualToDateByDayIgnoringTime(self.selectedDate!, day: day)
        })
    }

    //MARK: - Set up chart

    func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: ChartHighlight) {
    }

    func setChart() {

        barChartView.delegate = self
        barChartView.noDataText = "Please provide data for the chart."

        barChartView.infoTextColor = UIColor.whiteColor()
        barChartView.descriptionTextColor = UIColor.clearColor()

        barChartView.drawGridBackgroundEnabled = false
        barChartView.leftAxis.enabled = false
        barChartView.legend.enabled = false
        barChartView.rightAxis.enabled = false
        barChartView.leftAxis.drawGridLinesEnabled = false
        barChartView.rightAxis.drawGridLinesEnabled = false

        barChartView.xAxis.labelTextColor = UIColor.lightGrayColor()
        barChartView.xAxis.labelPosition = .Bottom
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.setScaleEnabled(false)
        barChartView.pinchZoomEnabled = false
        barChartView.autoScaleMinMaxEnabled = true

        barChartView.animate(xAxisDuration: 1.5, yAxisDuration: 2.0)
        barChartView.highlightPerDragEnabled = false
        barChartView.zoom(3.8, scaleY: 1.0, x: 0, y: 0)
    }

    func generateBarData() -> BarChartData {

        var dataEntries: [BarChartDataEntry] = []
        var dayStrings: [String] = []

        if let day = self.selectedDate?.monthDays() {

            for i in 1..<day {

                var amount = 0.0
                for ledger in self.getLedgers(i) {
                    amount = amount + ledger.amount!.doubleValue
                }

                let dataEntry = BarChartDataEntry(value: amount, xIndex: i - 1)
                dataEntries.append(dataEntry)
                dayStrings.append(String(i))
            }
        }
        
        let barChartSet: BarChartDataSet = BarChartDataSet(yVals: dataEntries, label: "收入")
        barChartSet.setColor(UIColor(red: 231/255, green: 18/255, blue: 0/255, alpha: 1))
        barChartSet.valueTextColor = UIColor.whiteColor()
        
        return BarChartData(xVals: dayStrings, dataSets: [barChartSet])
    }
}
