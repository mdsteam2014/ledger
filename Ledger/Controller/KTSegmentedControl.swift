//
//  ADVSegmentedControl.swift
//  Mega
//
//  Created by Alex Chen on 2016/6/20.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

@IBDesignable class KTSegmentedControl: UIControl {

    private var labels = [UILabel]()
    var thumbView = UIView()

    var thumbHeight: CGFloat = 0.0
    let dateLabel = UILabel()
    var items: [String] = ["item 1", "item 2", "item 2", "item 2", "item 2"] {
        didSet {
            setupLabels()
        }
    }

    var selectedIndex : Int = 0 {
        didSet {
            displayNewSelectedIndex()
        }
    }

    @IBInspectable var selectedLabelColor : UIColor = UIColor.blackColor() {
        didSet {
            setSelectedColors()
        }
    }

    @IBInspectable var unselectedLabelColor : UIColor = UIColor.whiteColor() {
        didSet {
            setSelectedColors()
        }
    }

    @IBInspectable var thumbColor : UIColor = UIColor.whiteColor() {
        didSet {
            setSelectedColors()
        }
    }

    @IBInspectable var borderColor : UIColor = UIColor.whiteColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }

    @IBInspectable var font : UIFont! = UIFont.systemFontOfSize(12) {
        didSet {
            setFont()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }

    func setupView(){

        layer.cornerRadius = frame.height / 2
        layer.borderColor = UIColor.grayColor().CGColor
        layer.borderWidth = 1
        backgroundColor = CustomColors.itemSeparatorBackgroundColor

        setupLabels()

        addIndividualItemConstraints(labels, mainView: self, padding: 0)

        insertSubview(thumbView, atIndex: 0)

    }

    func setupLabels(){

        for label in labels {
            label.removeFromSuperview()
        }

        labels.removeAll(keepCapacity: true)

        for index in 1...items.count {

            var selectFrame = self.bounds
            let newWidth = CGRectGetWidth(selectFrame) / CGFloat(items.count)
            selectFrame.size.width = newWidth - 20

            let label = UILabel(frame: selectFrame)
            label.text = items[index - 1]
            label.backgroundColor = UIColor.clearColor()
            label.textAlignment = .Center
            label.font = UIFont(name: "Avenir-Black", size: 10)
            label.textColor = index == 1 ? selectedLabelColor : unselectedLabelColor
            label.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(label)
            labels.append(label)
        }

        addIndividualItemConstraints(labels, mainView: self, padding: 0)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        var selectFrame = self.bounds
        let newWidth = CGRectGetWidth(selectFrame) / CGFloat(items.count)
        thumbHeight = selectFrame.size.height - 1
        selectFrame.size.width = newWidth
        selectFrame.size.height = thumbHeight
        thumbView.frame = selectFrame
        thumbView.backgroundColor = thumbColor
        thumbView.layer.cornerRadius = thumbHeight / 2

        thumbView.layer.borderColor = thumbColor.CGColor
        thumbView.layer.borderWidth = 2

        thumbView.layer.shadowColor = UIColor.blackColor().CGColor
        thumbView.layer.shadowOpacity = 1
        thumbView.layer.shadowOffset = CGSizeZero
        thumbView.layer.shadowRadius = 10
        thumbView.layer.shadowPath = UIBezierPath(rect: thumbView.bounds).CGPath
        thumbView.layer.shouldRasterize = true

        displayNewSelectedIndex()

    }

    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {

        let location = touch.locationInView(self)

        var calculatedIndex : Int?
        for (index, item) in labels.enumerate() {
            if item.frame.contains(location) {
                calculatedIndex = index
            }
        }


        if calculatedIndex != nil {
            selectedIndex = calculatedIndex!
            sendActionsForControlEvents(.ValueChanged)
        }

        return false
    }

    func displayNewSelectedIndex(){
        for item in labels {
            item.textColor = unselectedLabelColor
        }

        let label = labels[selectedIndex]
        label.textColor = selectedLabelColor

        let options: UIViewAnimationOptions = [.CurveEaseOut, .BeginFromCurrentState]

        UIView.animateWithDuration(0.3, delay: 0.0, options: options, animations: {

             self.thumbView.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.width, self.thumbHeight)

            /*
            if self.selectedIndex == 0 {

                self.thumbView.frame = CGRectMake(label.frame.origin.x + 2.0, label.frame.origin.y + 2.5, label.frame.width, self.thumbHeight)
            } else if self.selectedIndex == self.labels.count - 1 {
                self.thumbView.frame = CGRectMake(label.frame.origin.x - 2.0, label.frame.origin.y + 2.5, label.frame.width, self.thumbHeight)
            } else {

                self.thumbView.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y + 2.5, label.frame.width, self.thumbHeight)
            }*/


            }, completion: nil)
    }

    func addIndividualItemConstraints(items: [UIView], mainView: UIView, padding: CGFloat) {

        for (index, button) in items.enumerate() {

            let topConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: mainView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 0)

            let bottomConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: mainView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0)

            var rightConstraint : NSLayoutConstraint!

            if index == items.count - 1 {

                rightConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: mainView, attribute: NSLayoutAttribute.Right, multiplier: 1.0, constant: -padding)

            }else{

                let nextButton = items[index+1]
                rightConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: nextButton, attribute: NSLayoutAttribute.Left, multiplier: 1.0, constant: -padding)
            }


            var leftConstraint : NSLayoutConstraint!

            if index == 0 {

                leftConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: mainView, attribute: NSLayoutAttribute.Left, multiplier: 1.0, constant: padding)

            }else{

                let prevButton = items[index-1]
                leftConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: prevButton, attribute: NSLayoutAttribute.Right, multiplier: 1.0, constant: padding)

                let firstItem = items[0]

                let widthConstraint = NSLayoutConstraint(item: button, attribute: .Width, relatedBy: NSLayoutRelation.Equal, toItem: firstItem, attribute: .Width, multiplier: 1.0  , constant: 0)

                mainView.addConstraint(widthConstraint)
            }
            
            mainView.addConstraints([topConstraint, bottomConstraint, rightConstraint, leftConstraint])
        }
    }
    
    func setSelectedColors(){
        for item in labels {
            item.textColor = unselectedLabelColor
        }
        
        if labels.count > 0 {
            labels[0].textColor = selectedLabelColor
        }
        
        thumbView.backgroundColor = thumbColor
    }
    
    func setFont(){
        for item in labels {
            item.font = font
        }
    }
}
