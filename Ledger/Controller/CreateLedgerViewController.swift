//
//  ExpenseViewController.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/26.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

class CreateLedgerViewController: FormViewController, CalculatorViewDelegate {


    //MARK: - Local Property

    let createHeader: (() -> ViewFormer) = {
        return CustomViewFormer<FormHeaderFooterView>()
            .configure {
                $0.viewHeight = 20
        }
    }

    var titleRow: TextFieldRowFormer<FormTextFieldCell>?
    let calculatorView = CalculatorView()
    var categoryRow: RowFormer?
    var navHeight: CGFloat = 0
    var ledger: Ledger?

    @IBOutlet weak var addButton: UIBarButtonItem!
    //@IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var segmentControl: KTSegmentedControl!

    var currentDate: NSDate?

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        calculatorView.delegate = self
        configure()
    }

    override func viewWillAppear(animated: Bool) {

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background")!, forBarMetrics: UIBarMetrics.Default)
        self.view.backgroundColor = CustomColors.backgroundColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func segmentValueChanged(sender: AnyObject) {
        buildCreateRow()
    }
    
    func buildCreateRow() {
        former.removeUpdate(rowFormer: categoryRow!, rowAnimation: .Fade)
        if segmentControl.selectedIndex == 0 {
            categoryRow = createCategoryRow("支出類別", "支出類別", pushCategoryRowSelected("請選擇支出類別", height: navHeight))

        } else {
            categoryRow = createCategoryRow("收入類別",  "收入類別", pushCategoryRowSelected("請選擇收入類別", height: navHeight))
        }
        ledger?.ledgerType = segmentControl.selectedIndex
        former.insertUpdate(rowFormer: categoryRow!, toIndexPath: NSIndexPath(forRow: 0, inSection: 0), rowAnimation: .Fade)
    }

    // MARK: - Calculator delegate

    func calculatorKeyDown(number: String) {

        if (ledger?.categoryTypeId) != nil {
            if number == "" {
                self.addButton.enabled = false
            } else {
                self.addButton.enabled = true
            }
        }

        self.titleRow?.cell.textField.text = number.formatCurrencyStyle
        ledger?.amount = Double(number)
    }

    func calculatorDone() {
        titleRow?.cell.textField.resignFirstResponder()
    }

    private func configure() {

        addButton.enabled = false
        //segmentControl!.tintColor = UIColor.whiteColor()
        segmentControl.items = ["支出", "收入"]
        segmentControl.selectedIndex = 0
        tableView.contentInset.bottom = 30
        tableView.contentOffset.y = -10
        tableView.backgroundColor = CustomColors.backgroundColor
        tableView.separatorColor = CustomColors.itemSeparatorBackgroundColor

        ledger = CoreDataHelper.sharedInstance.dataContext.ledger.createEntity()

        if segmentControl.selectedIndex == 0 {
            categoryRow = createCategoryRow("支出類別", "支出類別", pushCategoryRowSelected("請選擇支出類別", height: navHeight))
        } else {
            categoryRow = createCategoryRow("收入類別",  "收入類別", pushCategoryRowSelected("請選擇收入類別", height: navHeight))
        }

        ledger?.ledgerType = segmentControl.selectedIndex
        ledger?.date = self.currentDate
        ledger?.exchangeTypeId = CoreDataHelper.sharedInstance.dataContext.exchangeType.toArray().first?.id

        titleRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "金額"
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.textField.textColor = UIColor.whiteColor()
            $0.textField.font = .systemFontOfSize(15)
            $0.textField.keyboardType = UIKeyboardType.NumberPad
            $0.textField.inputView = self.calculatorView.textFieldInputView
            }.configure {
                $0.attributedPlaceholder = NSAttributedString(string:"請輸入金額",
                    attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
        }

        let dateRow = InlineDatePickerRowFormer<FormInlineDatePickerCell>() {
            $0.titleLabel.text = "日期"
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.displayLabel.textColor = UIColor.whiteColor()
            $0.displayLabel.font = .boldSystemFontOfSize(15)
            }.inlineCellSetup {
                $0.datePicker.datePickerMode = .DateAndTime
                $0.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
            }.configure {
                $0.displayEditingColor = UIColor.lightGrayColor()
                if self.currentDate != nil {
                    $0.date = self.currentDate!
                }
            }.displayTextFromDate(String.mediumDateShortTime).onDateChanged { (date) in
                self.ledger?.date = date
        }

        let banksRow = InlinePickerRowFormer<FormInlinePickerCell, BankAccount>() {
            $0.titleLabel.text = "銀行"
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.displayLabel.textColor = UIColor.whiteColor()
            $0.displayLabel.font = .systemFontOfSize(15)
            $0.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
            }.configure {
                $0.displayEditingColor = UIColor.lightGrayColor()
                let banks = CoreDataHelper.sharedInstance.dataContext.bankAccount.toArray()
                for bank in banks {
                    let title = "\(bank.bankName!) - \(String(bank.amount!).formatCurrencyStyle)"

                    let attributed = NSAttributedString(string: bank.bankName!,
                        attributes: [NSForegroundColorAttributeName: UIColor.lightGrayColor()])

                    let item = InlinePickerItem(title: title, displayTitle: attributed, value: bank)
                    $0.pickerItems.append(item)
                }

                self.ledger?.bankId = banks.filter({
                    $0.index == 0
                }).first?.id
            }.onValueChanged {

                print($0.value?.id)
                self.ledger?.bankId = $0.value?.id
        }

        let noteRow = TextViewRowFormer<FormTextViewCell>() {
            $0.textView.textColor = UIColor.whiteColor()
            $0.textView.font = .systemFontOfSize(15)
            }.configure {
                $0.attributedPlaceholder = NSAttributedString(string:"備註",
                    attributes:[NSForegroundColorAttributeName: UIColor.grayColor()])
            }.onTextChanged {
                self.ledger?.note = $0
        }

        let categorySection = SectionFormer(rowFormer:categoryRow!)
            .set(headerViewFormer: createHeader())

        let titleSection = SectionFormer(rowFormer:titleRow!, dateRow)
            .set(headerViewFormer: createHeader())

        let exchangeSection = SectionFormer(rowFormer: banksRow, createExchangeRow())
            .set(headerViewFormer: createHeader())

        let titleSection2 = SectionFormer(rowFormer: noteRow)
            .set(headerViewFormer: createHeader())

        former.append(sectionFormer: categorySection)
        former.append(sectionFormer: titleSection)
        former.append(sectionFormer: exchangeSection)
        former.append(sectionFormer: titleSection2)
    }

    // MARK: - Create Row

    let createCategoryRow = {
        ( text: String, subText: String, onSelected: (RowFormer -> Void)?) -> RowFormer in

        return LabelRowFormer<FormLabelCell>() {
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.subTextLabel.textColor = UIColor.whiteColor()
            $0.subTextLabel.font = .boldSystemFontOfSize(15)
            $0.accessoryType = .DisclosureIndicator
            }.configure { form in
                _ = onSelected.map { form.onSelected($0) }
                form.text = text
                form.subText = subText
        }
    }

    func pushCategoryRowSelected(title: String, height: CGFloat) -> RowFormer -> Void {
        return { [weak self] rowFormer in
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TypeSelectorViewContoller()
                controller.setHeight = height
                controller.title = title
                controller.type = (self?.segmentControl.selectedIndex)!
                controller.selectedText = rowFormer.subText
                controller.onSelected = {
                    self?.former.deselect(true)
                    self?.ledger?.categoryTypeId = $0.id
                    self?.ledger?.categoryTypeRootId = $0.rootId
                    rowFormer.subText = $0.name
                    rowFormer.update()

                    if (self!.ledger?.amount) != nil {
                        if self!.titleRow?.cell.textField.text == "" {
                            self!.addButton.enabled = false
                        } else {
                            self!.addButton.enabled = true
                        }
                    }
                }
                self?.former.deselect(true)
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }

    func createExchangeRow() -> InlinePickerRowFormer<FormInlinePickerCell, ExchangeType> {

        let exchangeTypes = CoreDataHelper.sharedInstance.dataContext.exchangeType.toArray()

        let row = InlinePickerRowFormer<FormInlinePickerCell, ExchangeType>() {
            $0.titleLabel.text = "交易方式"
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.displayLabel.textColor = UIColor.whiteColor()
            $0.displayLabel.font = .systemFontOfSize(15)
            $0.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
            }.configure {

                $0.displayEditingColor = UIColor.lightGrayColor()
                
                for exchange in exchangeTypes {
                    $0.pickerItems.append(
                        InlinePickerItem(title: exchange.name!,
                            displayTitle: NSAttributedString(string:  exchange.name!,
                                attributes: [NSForegroundColorAttributeName: UIColor.lightGrayColor()]),
                            value: exchange)
                    )
                }
            }.onValueChanged {
                self.ledger?.bankId = $0.value?.id
        }
        return row
    }
}
