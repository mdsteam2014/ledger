//
//  SettingViewControllerTableViewController.swift
//  Ledger
//
//  Created by Alex Chen on 2016/6/20.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import UIKit

class SettingViewController: FormViewController {

    let createHeader: (() -> ViewFormer) = {
        return CustomViewFormer<FormHeaderFooterView>()
            .configure {
                $0.viewHeight = 20
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = CustomColors.backgroundColor
        configure()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {

        for parent in self.navigationController!.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background")!, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        self.view.backgroundColor = CustomColors.backgroundColor
    }

    private func configure() {

        tableView.contentInset.bottom = 30
        tableView.contentOffset.y = -10
        tableView.backgroundColor = CustomColors.backgroundColor
        tableView.separatorColor = CustomColors.itemSeparatorBackgroundColor


        let budgetRow = LabelRowFormer<FormLabelCell>() {
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.subTextLabel.textColor = UIColor.whiteColor()
            $0.accessoryType = .DisclosureIndicator

            }.configure { form in
                form.text = "編列預算"
        }

        let categoryRow = LabelRowFormer<FormLabelCell>() {
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.subTextLabel.textColor = UIColor.whiteColor()
            $0.accessoryType = .DisclosureIndicator

            }.configure { form in
                form.text = "類別管理"
        }

        let accountRow = LabelRowFormer<FormLabelCell>() {
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.subTextLabel.textColor = UIColor.whiteColor()
            $0.accessoryType = .DisclosureIndicator

            }.configure { form in
                form.text = "帳戶管理"
        }

        let passwordRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "密碼設定"
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.switchButton.onTintColor = CustomColors.SwitchTintColorColor
            }.configure {
                //$0.switched = Profile.sharedInstance.moreInformation
                $0.switchWhenSelected = true
            }.onSwitchChanged {_ in
        }

        let aboutRow = LabelRowFormer<FormLabelCell>() {
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.subTextLabel.textColor = UIColor.whiteColor()
            $0.accessoryType = .None

            }.configure { form in
                form.text = "關於我們"
        }

        let rankRow = LabelRowFormer<FormLabelCell>() {
            $0.titleLabel.textColor = UIColor.whiteColor()
            $0.subTextLabel.textColor = UIColor.whiteColor()
            $0.accessoryType = .None

            }.configure { form in
                form.text = "給予評分"
        }

        let section = SectionFormer(rowFormer: categoryRow, budgetRow, accountRow)
            .set(headerViewFormer: createHeader())
        let section1 = SectionFormer(rowFormer: passwordRow)
            .set(headerViewFormer: createHeader())

        let section2 = SectionFormer(rowFormer: aboutRow, rankRow)
            .set(headerViewFormer: createHeader())
        former.append(sectionFormer: section)
        former.append(sectionFormer: section1)

        former.append(sectionFormer: section2)
    }

}
