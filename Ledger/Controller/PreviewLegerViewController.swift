//
//  PreviewLegerViewController.swift
//  Ledger
//
//  Created by Alex Chen on 2016/7/8.
//  Copyright © 2016年 Alex.Chen. All rights reserved.
//

import Foundation
import UIKit

class PreviewLegerViewController:  UIViewController {

    @IBOutlet weak var categoryTypeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var exchangeTypeLabel: UILabel!
    @IBOutlet weak var noteTextView: UITextView!

    var selectedLedger: Ledger?
    override func viewDidLoad() {
        super.viewDidLoad()
        if let type = selectedLedger?.categoryTypeId {
            self.categoryTypeLabel.text = CoreDataHelper.sharedInstance.dataContext.categoryType.first({_ in
                NSPredicate(format: "id = %@", type)
            })?.name
        }

        if let amount = selectedLedger?.amount {
            self.amountLabel.text = "\(amount)".formatCurrencyStyle
        }

        if let date = selectedLedger?.date {
            self.dateLabel.text = date.toString(dateStyle: .FullStyle, timeStyle: .NoStyle)
            self.timeLabel.text = date.toString(dateStyle: .NoStyle, timeStyle: .MediumStyle)
        }

        if let bankId = selectedLedger?.bankId {
            self.bankLabel.text = CoreDataHelper.sharedInstance.dataContext.bankAccount.first({_ in
                NSPredicate(format: "id = %@", bankId)
            })?.bankName
        }

        if let exchangeTypeId = selectedLedger?.exchangeTypeId {
            self.exchangeTypeLabel.text = CoreDataHelper.sharedInstance.dataContext.exchangeType.first({_ in
                NSPredicate(format: "id = %@", exchangeTypeId)
            })?.name
        }

        if let note = selectedLedger?.note {
            self.noteTextView.text = note
        } else {
            self.noteTextView.text = ""
        }
        self.title = selectedLedger?.ledgerType == 0 ? "支出明細" : "收入明細"

    }
    override func viewWillAppear(animated: Bool) {

        for parent in self.navigationController!.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background")!, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        self.view.backgroundColor = CustomColors.backgroundColor
    }

    @IBAction func deleteLedger(sender: AnyObject) {
        if let ledger = self.selectedLedger {
            CoreDataHelper.sharedInstance.dataContext.ledger.deleteEntity(ledger)
            do {
                try CoreDataHelper.sharedInstance.dataContext.save()
                self.navigationController?.popViewControllerAnimated(true)
            } catch {
                print(error)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}