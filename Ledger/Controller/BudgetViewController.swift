//
//  SecondViewController.swift
//  Ledger
//
//  Created by Alex Chen on 2016/4/6.
//  Copyright © 2016 Alex.Chen. All rights reserved.
//

import UIKit

class BudgetViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, KTCalendarHeaderDelegate, BudgetCollectionDelegate {

    @IBOutlet weak var budgetPercentage: MKRingProgressView!
    @IBOutlet weak var calendarHeader: KTCalendarHeader!
    @IBOutlet weak var cView: UIView!
    @IBOutlet weak var totalBudget: KTButton!
    @IBOutlet weak var totalExpense: KTButton!
    var collectionView: UICollectionView!
    var selectedType: CategoryType?
    var selectedDate: NSDate?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.calendarHeader.delegate = self

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 320, height: self.cView.frame.size.height)
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;

        layout.scrollDirection = .Horizontal
        self.collectionView = UICollectionView(frame: CGRectMake(0, 0, self.cView.frame.size.width, self.cView.frame.size.height), collectionViewLayout: layout)
        self.collectionView.pagingEnabled = true
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.registerNib(UINib(nibName: "BudgetViewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "budgetViewCollectionViewCell")
        self.collectionView.backgroundColor = UIColor.clearColor()
        self.cView.addSubview(collectionView)

        self.collectionView.performBatchUpdates({
            self.collectionView?.scrollToItemAtIndexPath(self.calendarHeader.selectedIndex!, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
            }, completion: nil)
    }

    override func viewWillAppear(animated: Bool) {
        for parent in self.navigationController!.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background")!, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        self.view.backgroundColor = CustomColors.backgroundColor
        self.collectionView.reloadData()
    }

    override func viewDidAppear(animated: Bool) {
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.0)
        CATransaction.commit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let minimumPage = NSDate().beginingOfMonthOfDate(NSDate().dateBySubtractingMonths(24))
        let count = NSDate().monthsFromDate(minimumPage, toDate: NSDate()) + 1
        return count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("budgetViewCollectionViewCell", forIndexPath: indexPath) as! BudgetViewCollectionViewCell

        cell.delgate = self

        selectedDate = NSDate().dateByAddingMonths(indexPath.row, toDate: NSDate().dateBySubtractingMonths(24))
        let items = CoreDataHelper.sharedInstance.getBudgetByMonth(selectedDate!)
        cell.items = items

        var expenseAmount = 0.0
        for item in items {
            expenseAmount = expenseAmount + item.amount!.doubleValue
        }

        var budgetAmount = 0.0
        for budget in CoreDataHelper.sharedInstance.dataContext.budget {
            budgetAmount = budgetAmount + budget.amount!.doubleValue
        }

        // TODO: fake data
        budgetAmount = 10000

        self.totalBudget.setTitle(String(budgetAmount).formatCurrencyStyle, forState: .Normal)
        self.totalExpense.setTitle(String(expenseAmount).formatCurrencyStyle, forState: .Normal)
        var percentage = 0.0
        percentage = expenseAmount / budgetAmount
        budgetPercentage.progress = percentage

        cell.reloadTableView()

        return cell
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(320, self.cView.frame.size.height)
    }

    //MARK: - UIScrollViewDelegate

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let visibleRect = CGRect(origin:  self.collectionView.contentOffset, size: self.collectionView.bounds.size)
        let visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect))
        if let visibleIndexPath = self.collectionView.indexPathForItemAtPoint(visiblePoint) {
            calendarHeader.resetNewOffset(visibleIndexPath.row)
        }
    }

    func headerScrollViewDidEndDecelerating(index: Int) {
        let lastItemIndex = NSIndexPath(forItem: index, inSection: 0)
        self.collectionView?.scrollToItemAtIndexPath(lastItemIndex, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
    }


    // MARK: - BudgetCollectionDelegate
    func budgetCollectionSelectedItem(indexPath: NSIndexPath, categoryType: CategoryType) {
        self.selectedType = categoryType
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.performSegueWithIdentifier("goToBudgetDetail", sender: self)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        if let view = segue.destinationViewController as? BudgetDetailViewController {
            view.selectedCategoryType = self.selectedType
            view.selectedDate = self.selectedDate
        }
    }
}

